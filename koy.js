'use strict';

// Poor man's dependency injection:
var Context = {};

// Data lives here for until the bot is terminated. Can be used to allow
// listeners to be aware of the state of other listeners or to enact some sort
// of signalling. Be careful to not dump data here and forget about it as the
// bot's memory footprint will continue to grow.

// Combining this with the key-value store will allow state to persist between
// restarts of the bot.

Context.memory = {};

require('./lib/function.js');
Context.net = require('net');
Context.sqlite3 = require('sqlite3');
Context.fs = require('fs');
Context.spawn = require('child_process').spawn;
Context.https = require('https');
Context.http = require('http');
Context.fmt = require('util').format;
Context.moment = require('moment');
Context.xml = require('xmldoc');
Context.ent = require('ent');
Context.zlib = require('zlib');

Context.HelpText = {};
Context.DateUtils = require('./lib/date_utils.js');
Context.MathsUtils = require('./lib/maths_utils.js');
Context.FileUtils = require('./lib/file_utils.js');
Context.Admin = require('./admin.js');

Context.cmdLine = {
	prettyLogs: false
	, tui: false
	, debug: false
};

var args = process.argv;
args.shift();
args.shift();

if (args.length > 0) {
	args.forEach(function (arg) {
		switch (arg) {
			case '--pretty-logs':
				Context.cmdLine.prettyLogs = true;
				break;

			case '--tui':
				Context.cmdLine.prettyLogs = true;
				Context.cmdLine.tui = true;
				break;

			case '--debug':
				Context.cmdLine.debug = true;
				break;

			default:
				console.error(
					Context.fmt("Unknown command line option '%s'.", arg));
		}
	});
}

var Log = require('./lib/log.js');
Context.Log = new Log(Context.cmdLine.tui);

Context.config = null;
Context.maindb = null;
Context.logdb = null;

try {
	Context.config = JSON.parse(Context.fs.readFileSync('conf.json'));
} catch (e) {}

if (Context.config == null) {
	Context.Log.error('Unable to read config file.');
	process.exit(1);
}

var DatabaseLoader = require('./lib/database_loader.js');
var EventProcessor = require('./lib/event_processor.js');
var dAmn = require('./lib/damn.js');
var TUIBridge = require('./lib/tui-bridge.js');
var ListenerLoader = require('./lib/listener_loader.js');
var OAuth = require('./lib/oauth.js');
var Packet = require('./lib/packet.js');
var Recv = require('./lib/recv.js');
var Property = require('./lib/property.js');
var Locks = require('./lib/locks.js');
var KeyValueStore = require('./lib/key_value_store.js');
var Tablumps = require('./lib/tablumps.js');

Context.TUIBridge = new TUIBridge(Context);
Context.OAuth = new OAuth(Context);
Context.Packet = new Packet(Context);
Context.Tablumps = new Tablumps(Context);
Context.DatabaseLoader = new DatabaseLoader(Context);
Context.Locks = new Locks(Context);

Context.client = null;

var connect = function () {
	Context.Log.log('Connection established.');
	dAmn.handshake();
};

var data = function (data) {
	var dataStr = data.toString();
	if (!Context.cmdLine.prettyLogs) {
		Context.Log.log(dataStr);
	}
	dAmn.recv(dataStr);
};

var end = function (end) {
	Context.Log.log('Connection closed.');
	exitHandler();
};

var main = function () {
	EventProcessor = new EventProcessor(Context);

	Context.KeyValueStore = new KeyValueStore(Context);
	Context.Recv = new Recv(Context, EventProcessor);
	Context.Property = new Property(Context, EventProcessor);

	dAmn = new dAmn(Context, EventProcessor);
	ListenerLoader = new ListenerLoader(Context, EventProcessor, dAmn);
	ListenerLoader.load('listeners');

	// After all listeners are loaded and the bot is fully initialised, we fire
	// an event to allow listeners to perform their own initialisation.
	EventProcessor.fire('init');

	Context.client = Context.net.connect(
		{port: 3900, host: 'chat.deviantart.com'}
		, connect);
	Context.client.on('data', data);
	Context.client.on('end', end);
};

var exitHandler = function () {
	Context.Log.log('Shutdown initiated, closing databases.');

	if (Context.maindb == null) {
		process.exit();
	}

	Context.maindb.close(function (err) {
		if (err != null) {
			Context.Log.error('Error closing main DB: ' + err.message);
		}

		if (Context.logdb == null) {
			process.exit();
		}

		Context.logdb.close(function (err) {
			if (err != null) {
				Context.Log.error('Error closing log DB: ' + err.message);
			}

			process.exit();
		});
	});
};

process.on('SIGINT', exitHandler);
process.on('uncaughtException', function (err) {
	Context.Log.error(err.stack);
	exitHandler();
});

Context.DatabaseLoader.load('db/main.sq3', 'db/main.sql', function (db) {
	if (db == null) {
		Context.Log.error(
			'Unable to load main database. The bot cannot start up.');

		return;
	}

	Context.maindb = db;
	Context.DatabaseLoader.load('db/log.sq3', 'db/log.sql', function (db) {
		if (db == null) {
			Context.maindb.close();
			Context.Log.error(
				'Unable to load log database. The bot cannot start up.');

			return;
		}

		Context.logdb = db;
		main();
	});
});
