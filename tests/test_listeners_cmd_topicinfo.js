'use strict';

var topicinfo = require('../listeners/cmd_topicinfo.js');
var utils = require('../lib/test_utils.js');

exports.testPropertyTopic = function (test) {
	test.expect(4);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor('property_topic', {by: 'KoYbot'});

	Context.KeyValueStore = {};
	Context.memory = {};
	Context.config = {};
	Context.config.name = 'KoYbot';

	new topicinfo(Context, EventProcessor);
	test.strictEqual(Context.memory.lastTopicChange, undefined);

	Context.config.name = 'not KoYbot';
	Context.KeyValueStore.put = function (key, value, cb) {
		test.strictEqual(key, 'LastTopicChange');
		test.strictEqual(value.by, 'KoYbot');
	};

	new topicinfo(Context, EventProcessor);
	test.strictEqual(Context.memory.lastTopicChange.by, 'KoYbot');

	test.done();
};

exports.testTopicinfo = function (test) {
	test.expect(2);

	var Context =  utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor('cmd_topicinfo', {from: 'a', channel: 'chan'});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	Context.memory = {};
	new topicinfo(Context, EventProcessor, dAmn);

	Context.memory.lastTopicChange = {
		by: 'b'
		, at: Context.moment('2000-01-01').utc().valueOf()
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(
			msg
			, 'The topic was last changed by b on 01 Jan 2000 00:00:00.');
	};

	new topicinfo(Context, EventProcessor, dAmn);

	test.done();
};

exports.testReloadState = function (test) {
	test.expect(4);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor('init', {});

	Context.memory = {};
	Context.KeyValueStore = {};
	Context.KeyValueStore.get = function (key, cb) {
		test.strictEqual(key, 'LastTopicChange');
		cb('undefined');
	};

	new topicinfo(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Corrupt JSON retrieved when loading '
		+ 'last topic state: undefined\nUnexpected token u');

	Context.KeyValueStore.get = function (key, cb) {
		test.strictEqual(key, 'LastTopicChange');
		cb('{"by":"a","at":666}');
	};

	new topicinfo(Context, EventProcessor);
	test.deepEqual(Context.memory.lastTopicChange, {by: 'a', at: 666});

	test.done();
};
