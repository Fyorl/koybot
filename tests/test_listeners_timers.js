'use strict';

require('../lib/function.js');
var timers = require('../listeners/timers.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');

exports.testInitError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor('init', {});

	Context.config = {};
	Context.config.name = 'bot';
	Context.maindb.all = testUtils.injectError();

	new timers(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error loading pending timers on startup: bad');

	test.done();
};

exports.testExecuteTimer = function (test) {
	var self = this;
	test.expect(18);

	var runs = 0;
	var Context = testUtils.getMockContext(self);
	var EventProcessor = testUtils.getMockEventProcessor('init', {});

	Context.config = {};
	Context.Recv = {};
	Context.Recv.Actions = {MSG: 'msg'};
	Context.config.name = 'bot';
	Context.config.cmdChar = ['!'];

	Context.maindb.all = function (query, fn) {
		fn(null, [
			{channel: 'chan', from: 'a', timestamp: 0, cmd: 'quote', id: 1}
			, {
				channel: 'chan'
				, from: 'a'
				, timestamp: 0
				, cmd: 'say hello'
				, id: 2
			}, {
				channel: 'chan'
				, from: 'a'
				, timestamp: 0
				, cmd: 'reply u wot m8?'
				, id: 3
			}
		]);
	};

	Context.maindb.run = function (query, values, fn) {
		fn({message: 'bad'});
		test.strictEqual(
			self.error
			, Context.fmt(
				'Error removing timer #%d from the database: bad'
				, values.$id));
	};

	EventProcessor.fire = function (evt, packet) {
		test.strictEqual(packet.channel, 'chan');
		test.strictEqual(packet.from, 'a');
		test.strictEqual(packet.action, 'msg');

		runs++;
		if (runs === 1) {
			test.strictEqual(evt, 'cmd_quote');
			test.strictEqual(packet.content, '!quote');
		} else if (runs === 2) {
			test.strictEqual(evt, 'cmd_say');
			test.strictEqual(packet.content, '!say hello');
		} else if (runs === 3) {
			test.strictEqual(evt, 'cmd_reply');
			test.strictEqual(packet.content, '!reply u wot m8?');
			test.done();
		}
	};

	new timers(Context, EventProcessor);
};

exports.testCmdAtInvalid = function (test) {
	test.expect(0);

	var inputs = ['!at', '!at ', '!at 00:00', '!at one o\'clock'];
	var Context = testUtils.getMockContext(this);

	Context.config = {name: 'bot'};
	Context.maindb.run = function (query, values, fn) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor =
			testUtils.getMockEventProcessor('cmd_at', {content: input});

		new timers(Context, EventProcessor);
	});

	test.done();
};

exports.testCmdInInvalid = function (test) {
	test.expect(0);

	var inputs = [
		'!in'
		, '!in '
		, '!in 1h'
		, '!in 1h '
		, '!in say hello'
		, '!in and !at are the interesting ones'
	];

	var Context = testUtils.getMockContext(this);

	Context.config = {name: 'bot'};
	Context.maindb.run = function (query, values, fn) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor =
			testUtils.getMockEventProcessor('cmd_in', {content: input});

		new timers(Context, EventProcessor);
	});

	test.done();
};

exports.testCalculateAbsoluteTime = function (test) {
	test.expect(9);

	var inputs = [
		'22:22'
		, '01/05'
		, '01-05'
		, '22:00:59'
		, '01/05/2000'
		, '01/01 01:01:01'
		, '01/01/2001 11:10'
		, '10-01 10:01'
		, '2001-10-01 10:01:10'
	];

	var outputs = [
		'2000-01-01T22:22:00.000Z'
		, '2000-05-01T00:00:00.000Z'
		, '2000-01-05T00:00:00.000Z'
		, '2000-01-01T22:00:59.000Z'
		, '2000-05-01T00:00:00.000Z'
		, '2000-01-01T01:01:01.000Z'
		, '2001-01-01T11:10:00.000Z'
		, '2000-10-01T10:01:00.000Z'
		, '2001-10-01T10:01:10.000Z'
	];

	var runs = 0;
	var Context = testUtils.getMockContext(this);
	var realMoment = Context.moment;

	Context.config = {name: 'bot'};
	Context.moment = testUtils.getMockMoment('2000-01-01T00:00:00.000Z');
	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(
			realMoment(values.$timestamp).utc().toISOString()
			, outputs[runs]);

		runs++;
	};

	inputs.forEach(function (input) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_at'
			, {content: '!at ' + input + ' say hello'});

		new timers(Context, EventProcessor);
	});

	test.done();
};

exports.testCalculateRelativeTime = function (test) {
	test.expect(8);

	var inputs = [
		'1d'
		, '1d2h'
		, '1d2h3m'
		, '1d2h3m4s'
		, '1d3m4s'
		, '2h1s'
		, '10s'
		, '32d10h11s'
	];

	var outputs = [
		'2000-01-02T00:00:00.000Z'
		, '2000-01-02T02:00:00.000Z'
		, '2000-01-02T02:03:00.000Z'
		, '2000-01-02T02:03:04.000Z'
		, '2000-01-02T00:03:04.000Z'
		, '2000-01-01T02:00:01.000Z'
		, '2000-01-01T00:00:10.000Z'
		, '2000-02-02T10:00:11.000Z'
	];

	var runs = 0;
	var Context = testUtils.getMockContext(this);
	var realMoment = Context.moment;

	Context.config = {name: 'bot'};
	Context.moment = testUtils.getMockMoment('2000-01-01T00:00:00.000Z');
	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(
			realMoment(values.$timestamp).utc().toISOString()
			, outputs[runs]);

		runs++;
	};

	inputs.forEach(function (input) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_in'
			, {content: '!in ' + input + ' say hello'});

		new timers(Context, EventProcessor);
	});

	test.done();
};

exports.testAddTimerError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_in'
		, {content: '!in 1h quote'});

	var values = {
		$channel: undefined
		, $from: undefined
		, $timestamp: Context.moment('2000-01-01T01:00:00.000Z').utc().valueOf()
		, $cmd: 'quote'
	};

	Context.config = {name: 'bot'};
	Context.maindb.run = testUtils.injectError();
	Context.moment = testUtils.getMockMoment('2000-01-01T00:00:00.000Z');

	new timers(Context, EventProcessor);
	test.strictEqual(
		this.error
		, Context.fmt(
			'Unable to store timer information in database: %j\nbad'
			, values));

	test.done();
};

exports.testAddTimerSuccess = function (test) {
	test.expect(1);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_in'
		, {content: '!in 1h quote', from: 'a'});

	var realMoment = Context.moment;
	Context.Recv = {Actions: {MSG: 'msg'}};
	Context.config = {name: 'bot', cmdChar: ['!']};
	Context.moment = testUtils.getMockMoment('2000-01-01T00:00:00.000Z');
	Context.maindb.run = function (query, values, fn) {
		if (values.$id !== undefined) {
			return;
		}

		// We have to reset moment here to the actual moment library so that
		// setTimer reads the current timestamp correctly and calculates the
		// timestamp difference as a negative number since 2000-01-01 is so far
		// in the past.

		// Passing a negative timestamp to setTimeout means it will be called
		// immediately on the next event loop tick instead of us having to wait
		// 3,600,000ms for the test to terminate.

		Context.moment = realMoment;
		fn.call({lastID: 666});
	};

	EventProcessor.fire = function () {};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a: Timer set for 01 Jan 2000 01:00:00.');
	};

	new timers(Context, EventProcessor, dAmn);
	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Log = require('../lib/log.js');
		var Recv = require('../lib/recv.js');
		var Packet = require('../lib/packet.js');
		var DatabaseLoader = require('../lib/database_loader.js');

		self.Context = {};
		self.Context.HelpText = {};
		self.Context.config = {cmdChar: ['!'], name: 'bot'};

		self.Context.fs = require('fs');
		self.Context.moment = require('moment');
		self.Context.fmt = require('util').format;
		self.Context.sqlite3 = require('sqlite3');

		self.Context.Recv = new Recv();
		self.Context.Log = new Log(false);
		self.Context.Packet = new Packet(self.Context);
		self.Context.DateUtils = require('../lib/date_utils.js');

		dbUtils.setupDBs(self.Context, cb);
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, cb);
	},

	testTimerStored: function (test) {
		var self = this;
		test.expect(6);

		var dAmn = {};
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_at'
			, {content: '!at 2000-01-01 quote', from: 'a', channel: 'chan'});

		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			if (values.$id === undefined) {
				dbRun.call(self.Context.maindb, query, values, function (err) {
					test.strictEqual(err, null);
					self.Context.maindb.get(
						'SELECT * FROM timers'
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(row.id, 1);
							test.strictEqual(row.channel, 'chan');
							test.strictEqual(row.from, 'a');
							test.strictEqual(row.cmd, 'quote');
							test.done();
						}
					);
				});
			}
		};

		dAmn.sendMsg = function () {};
		EventProcessor.fire = function () {};
		new timers(self.Context, EventProcessor, dAmn);
	},

	testTimerDeleted: function (test) {
		var self = this;
		test.expect(3);

		var dAmn = {};
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_at'
			, {content: '!at 2000-01-01 quote', from: 'a', channel: 'chan'});

		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			if (values.$id === undefined) {
				dbRun.call(self.Context.maindb, query, values, fn);
			} else {
				dbRun.call(self.Context.maindb, query, values, function (err) {
					test.strictEqual(err, null);
					self.Context.maindb.get(
						'SELECT count(*) FROM timers'
						, function (err, row) {
							test.strictEqual(err, null);
							test.deepEqual(row, {'count(*)': 0});
							test.done();
						}
					);
				});
			}
		};

		dAmn.sendMsg = function () {};
		EventProcessor.fire = function () {};
		new timers(self.Context, EventProcessor, dAmn);
	}
};
