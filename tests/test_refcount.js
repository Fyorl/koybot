'use strict';

var RefCount = require('../lib/refcount.js');

exports.testRefCount = function (test) {
	test.expect(3);

	var r = RefCount.create();
	test.ok(!r.inUse());

	try {
		r.free();
	} catch (e) {
		test.strictEqual(e.message, 'RefCount was reduced to below 0.');
	}

	r.allocate();
	test.ok(r.inUse());

	test.done();
};
