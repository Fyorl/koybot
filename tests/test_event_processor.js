'use strict';

var fmt = require('util').format;
var EventProcessor = require('../lib/event_processor.js');
var utils = require('../lib/test_utils.js');

exports.tests = {
	setUp: function (cb) {
		var self = this;
		var Context = {};
		utils.interceptLogging(Context, self);

		self.evt = new EventProcessor(Context);
		self.error = null;
		cb();
	},

	testPassNonFunctionToRegisterFails: function (test) {
		test.expect(1);
		this.evt.register('test', []);
		test.strictEqual(
			this.error
			, 'Tried to register a non function listener.');
		test.done();
	},

	testFireMultipleListeners: function (test) {
		test.expect(2);

		var fn1 = function () {
			test.ok(true);
		};

		var fn2 = function () {
			test.ok(true);
		};

		this.evt.register('e', fn1);
		this.evt.register('e', fn2);

		this.evt.fire('e');

		test.done();
	},

	testListenerRecievesMultipleArguments: function (test) {
		test.expect(3);

		var fn = function (a, b, c) {
			test.strictEqual(a, 1);
			test.strictEqual(b, 2);
			test.strictEqual(c, 3);
		};

		this.evt.register('e', fn);
		this.evt.fire('e', 1, 2, 3);

		test.done();
	},

	testClear: function (test) {
		test.expect(0);

		var fn = function () {
			test.ok(false);
		};

		this.evt.register('e', fn);
		this.evt.clear();
		this.evt.fire('e', 1, 2, 3);

		test.done();
	},

	testCurrentListeners: function (test) {
		test.expect(1);

		this.evt.register('a', function () {});
		this.evt.register('a', function () {});
		this.evt.register('b', function () {});

		test.deepEqual(this.evt.currentListeners(), ['a', 'b']);

		test.done();
	}
};
