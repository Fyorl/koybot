'use strict';

var store = require('../lib/key_value_store.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');

exports.testInitialiseError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	Context.maindb = null;

	var KeyValueStore = new store(Context);
	test.strictEqual(
		this.error
		, 'Unable to initialise key-value store as the '
		+ 'main database has not been opened.');

	test.done();
};

exports.testGetError = function (test) {
	var self = this;
	test.expect(8);

	var Context = testUtils.getMockContext(self);
	var KeyValueStore = new store(Context);

	var testValue = function (value) {
		test.strictEqual(value, null);
	};

	[function(){}, undefined, null].forEach(function (key) {
		KeyValueStore.get(key, testValue);
		test.strictEqual(
			self.error
			, 'Tried to pass invalid key to key-value lookup.');
	});

	Context.maindb.get = testUtils.injectError();
	KeyValueStore.get('test', testValue);
	test.strictEqual(
		self.error
		, 'Error performing key-value lookup of key "test": bad');

	test.done();
};

exports.testPutError = function (test) {
	var self = this;
	test.expect(12);

	var Context = testUtils.getMockContext(self);
	var KeyValueStore = new store(Context);

	var testValue = function (value) {
		test.strictEqual(value, false);
	};

	[
		[function(){}, null]
		, [undefined, null]
		, [null, null]
		, ['key', undefined]
		, ['key', function(){}]
	].forEach(function (args, i) {
		args.push(testValue);
		KeyValueStore.put.apply(KeyValueStore, args);

		if (i < 3) {
			test.strictEqual(
				self.error
				, 'Tried to pass invalid key to key-value store put.');
		} else {
			test.strictEqual(
				self.error
				, 'Tried to pass invalid value to '
				+ 'key-value store put for key "key".');
		}
	});

	Context.maindb.run = testUtils.injectError();
	KeyValueStore.put('key', 'value', testValue);
	test.strictEqual(
		self.error
		, 'Error updating key-value store for pair ("key", "value"): bad');

	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Log = require('../lib/log.js');
		var KeyValueStore = require('../lib/key_value_store.js');
		var DatabaseLoader = require('../lib/database_loader.js');

		self.Context = {};
		self.Context.fs = require('fs');
		self.Context.sqlite3 = require('sqlite3');
		self.Context.Log = new Log(false);
		self.Context.DatabaseLoader = new DatabaseLoader(self.Context);
		testUtils.interceptLogging(self.Context, self);

		dbUtils.setupDBs(self.Context, function () {
			self.Context.KeyValueStore = new KeyValueStore(self.Context);
			cb();
		});
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, cb);
	},

	testGet: function (test) {
		var self = this;
		test.expect(3);

		self.Context.KeyValueStore.get('key', function (value) {
			test.strictEqual(value, null);
		});

		self.Context.maindb.run(
			"INSERT INTO misc (key, value) VALUES ('key', 'value')"
			, function (err) {
				test.strictEqual(err, null);
				self.Context.KeyValueStore.get('key', function (value) {
					test.strictEqual(value, 'value');
				});
			}
		);

		test.done();
	},

	testPut: function (test) {
		var self = this;
		test.expect(6);

		var testReplace = function () {
			self.Context.KeyValueStore.put('key', 'value2', function (success) {
				test.strictEqual(success, true);
				self.Context.maindb.get(
					"SELECT value FROM misc WHERE key = 'key'"
					, function (err, row) {
						test.strictEqual(err, null);
						test.strictEqual(row.value, 'value2');
					}
				);
			});
		};

		self.Context.KeyValueStore.put('key', 'value', function (success) {
			test.strictEqual(success, true);
			self.Context.maindb.get(
				"SELECT value FROM misc WHERE key = 'key'"
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.value, 'value');
					testReplace();
				}
			);
		});

		test.done();
	}
};
