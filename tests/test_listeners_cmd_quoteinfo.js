'use strict';

var quoteinfo = require('../listeners/cmd_quoteinfo.js');
var utils = require('../lib/test_utils.js');

exports.testFiresFindquote = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor(
		{content: '!quoteinfo #69'});

	EventProcessor.fire = function (evt, recv) {
		test.strictEqual(evt, 'cmd_findquote');
		test.deepEqual(recv, {content: '!quoteinfo #69'});
	};

	Context.KeyValueStore = {get: function(){}};
	new quoteinfo(Context, EventProcessor);

	test.done();
};

exports.testNoMemory = function (test) {
	test.expect(0);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!quoteinfo'});

	Context.memory = {};
	Context.KeyValueStore = {get: function(){}};
	Context.maindb.get = function (query, values, fn) {
		test.ok(false);
	};

	new quoteinfo(Context, EventProcessor);

	test.done();
};

exports.testRetrieveQuoteError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!quoteinfo'});

	Context.memory = {recentQuoteID: 69};
	Context.KeyValueStore = {get: function(){}};
	Context.maindb.get = utils.injectError();

	new quoteinfo(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error retrieving quote info for quote #69: bad');

	test.done();
};

exports.testNoResults = function (test) {
	test.expect(0);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!quoteinfo'});

	Context.memory = {recentQuoteID: 69};
	Context.KeyValueStore = {get: function(){}};
	Context.maindb.get = function (query, values, fn) {
		fn(null, null);
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	new quoteinfo(Context, EventProcessor);
	test.done();
};

exports.testSuccess = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor(
		{content: '!quoteinfo', channel: 'chan'});

	Context.memory = {recentQuoteID: 69};
	Context.KeyValueStore = {get: function(){}};
	Context.maindb.get = function (query, values, fn) {
		fn(null, {
			id: 69
			, quoter: 'a'
			, time_quoted: 946684800000
			, quoted: 'b'
			, quote: 'quote me please!'
		});
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(
			msg
			, 'Quote #69, quoted by a on 01 Jan 2000 00:00:00: '
			+ '&lt;b&gt; quote me please!');
	};

	new quoteinfo(Context, EventProcessor, dAmn);
	test.done();
};

exports.testReloadState = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = {
		register: function (evt, fn) {
			if (evt === 'init') {
				fn();
			}
		}
	};

	Context.memory = {};
	Context.KeyValueStore = {};
	Context.KeyValueStore.get = function (key, cb) {
		test.strictEqual(key, 'RecentQuoteID');
		cb('42');
	};

	new quoteinfo(Context, EventProcessor);
	test.strictEqual(Context.memory.recentQuoteID, 42);
	test.done();
};
