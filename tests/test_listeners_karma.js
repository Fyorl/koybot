'use strict';

require('../lib/function.js');
var karma = require('../listeners/karma.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');

exports.testGiveKarmaInvalid = function (test) {
	test.expect(0);

	var inputs = ['!givekarma', '!givekarma '];
	var Context = testUtils.getMockContext(this);
	Context.Packet = testUtils.getMockPacket();

	Context.maindb.get = function (query, values, fn) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor =
			testUtils.getMockEventProcessor('cmd_givekarma', {content: input});

		new karma(Context, EventProcessor);
	});

	test.done();
};

exports.testModifyKarmaSelectError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test'});

	Context.maindb.get = testUtils.injectError();
	new karma(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error retrieving karma information for "test": bad');

	test.done();
};

exports.testFloodWarning = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test'});

	var fiveMinsAgo = Context.moment().utc().subtract(5, 'minutes').valueOf();
	Context.maindb.get = function (query, values, fn) {
		fn(null, {last_modified: fiveMinsAgo});
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'Karma for "test" has already been modified in '
			+ 'the last 15 minutes. Try again later.');
	};

	new karma(Context, EventProcessor, dAmn);
	test.done();
};

exports.testModifyKarmaUpdateError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = testUtils.injectError();
	new karma(Context, EventProcessor);
	test.strictEqual(this.error, 'Error updating karma value for "test": bad');
	test.done();
};

exports.testGiveKarmaSuccess = function (test) {
	var inputs = ['test', '"two words"', '"and three words"'];
	test.expect(inputs.length * 2);

	var Context = testUtils.getMockContext(this);

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(null, values.$id);
		fn();
	};

	var dAmn = {};
	var getSendMsg = function (expectMsg) {
		return function (channel, msg) {
			test.strictEqual(msg, expectMsg);
		};
	};

	inputs.forEach(function (input) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_givekarma'
			, {from: 'a', content: '!givekarma ' + input});

		dAmn.sendMsg = getSendMsg(
			Context.fmt(
				'a gave karma to "%s", bringing it to 1.'
				, input.replace(/"/g, '')));

		new karma(Context, EventProcessor, dAmn);
	});

	test.done();
};

exports.testGiveKarmaToSelf = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'me', content: '!givekarma me'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		fn();
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'me took karma from "me", bringing it to -1.');
	};

	new karma(Context, EventProcessor, dAmn);
	test.done();
};

exports.testGiveKarmaIncrementsExisting = function (test) {
	test.expect(2);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test'});

	Context.maindb.get = function (query, values, fn) {
		fn(null, {id: 666, item: 'test', karma: 665, last_modified: 1});
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(values.$id, 666);
		fn();
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a gave karma to "test", bringing it to 666.');
	};

	new karma(Context, EventProcessor, dAmn);
	test.done();
};

exports.testTakeKarma = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_takekarma'
		, {from: 'a', content: '!takekarma test'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		fn();
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a took karma from "test", bringing it to -1.');
	};

	new karma(Context, EventProcessor, dAmn);
	test.done();
};

exports.testAddReasonError = function (test) {
	test.expect(1);

	var dAmn = {sendMsg: function () {}};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test for doing the testing'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		if (values.$delta === undefined) {
			fn();
		} else {
			fn({message: 'bad'});
		}
	};

	new karma(Context, EventProcessor, dAmn);
	test.strictEqual(this.error, 'Error adding reason for item "test": bad');
	test.done();
};

exports.testAddReasonSuccess = function (test) {
	test.expect(8);

	var dAmn = {sendMsg: function () {}};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_givekarma'
		, {from: 'a', content: '!givekarma test for doing the testing'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		if (values.$delta === undefined) {
			fn();
		} else {
			test.strictEqual(values.$reason, 'for doing the testing');
			test.strictEqual(values.$item, 'test');
			test.strictEqual(values.$delta, 1);
			test.strictEqual(values.$user, 'a');
		}
	};

	new karma(Context, EventProcessor, dAmn);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_takekarma'
		, {from: 'a', content: '!takekarma test for failing the tests'});

	Context.maindb.run = function (query, values, fn) {
		if (values.$delta === undefined) {
			fn();
		} else {
			test.strictEqual(values.$reason, 'for failing the tests');
			test.strictEqual(values.$item, 'test');
			test.strictEqual(values.$delta, -1);
			test.strictEqual(values.$user, 'a');
		}
	};

	new karma(Context, EventProcessor, dAmn);

	test.done();
};

exports.testShortcuts = function (test) {
	test.expect(8);

	var dAmn = {};
	var inputs = ['test', '"two words"', '"and three words"'];
	var Context = testUtils.getMockContext(this);

	Context.config = {name: 'bot'};

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	Context.maindb.run = function (query, values, fn) {
		fn();
	};

	var getSendMsg = function (expected) {
		return function (channel, msg) {
			test.strictEqual(msg, expected);
		};
	};

	inputs.forEach(function (input) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'recvMsg'
			, {from: 'a', content: input + '++'});

		dAmn.sendMsg = getSendMsg(
			Context.fmt(
				'a gave karma to "%s", bringing it to 1.'
				, input.replace(/"/g, '')));

		new karma(Context, EventProcessor, dAmn);
	});

	inputs.forEach(function (input) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'recvMsg'
			, {from: 'a', content: input + '--'});

		dAmn.sendMsg = getSendMsg(
			Context.fmt(
				'a took karma from "%s", bringing it to -1.'
				, input.replace(/"/g, '')));

		new karma(Context, EventProcessor, dAmn);
	});

	dAmn.sendMsg = function () {};
	var EventProcessor = testUtils.getMockEventProcessor(
		'recvMsg'
		, {from: 'a', content: 'test++ for doing the testing'});

	Context.maindb.run = function (query, values, fn) {
		if (values.$delta === undefined) {
			fn();
		} else {
			test.strictEqual(values.$reason, 'for doing the testing');
			fn();
		}
	};

	new karma(Context, EventProcessor, dAmn);

	EventProcessor = testUtils.getMockEventProcessor(
		'recvMsg'
		, {from: 'a', content: '"two words"++ for doing the testing'});

	new karma(Context, EventProcessor, dAmn);

	test.done();
};

exports.testShortcutsIgnoreFromBot = function (test) {
	test.expect(0);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'recvMsg'
		, {from: 'bot', content: 'karma++'});

	Context.config = {name: 'bot'};

	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	new karma(Context, EventProcessor, dAmn);
	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Log = require('../lib/log.js');
		var Locks = require('../lib/locks.js');
		var Packet = require('../lib/packet.js');
		var DatabaseLoader = require('../lib/database_loader.js');

		self.Context = {};
		self.Context.fs = require('fs');
		self.Context.moment = require('moment');
		self.Context.sqlite3 = require('sqlite3');
		self.Context.fmt = require('util').format;
		self.Context.HelpText = {};
		self.Context.Log = new Log(false);
		self.Context.Locks = new Locks(self.Context);
		self.Context.Packet = new Packet(self.Context);
		self.Context.DatabaseLoader = new DatabaseLoader(self.Context);
		self.Context.config = {name: 'bot'};
		self.Context.cmdLine = {debug: true};
		testUtils.interceptLogging(self.Context, self);
		dbUtils.setupDBs(self.Context, cb);
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, cb);
	},

	testGiveKarmaItemNotExists: function (test) {
		var self = this;
		test.expect(4);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_givekarma'
			, {from: 'a', content: '!givekarma test'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM karma'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.item, 'test');
					test.strictEqual(row.karma, 1);
				}
			);
		};

		new karma(self.Context, EventProcessor, dAmn);
		test.done();
	},

	testTakeKarmaItemNotExists: function (test) {
		var self = this;
		test.expect(4);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_takekarma'
			, {from: 'a', content: '!takekarma test'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM karma'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.item, 'test');
					test.strictEqual(row.karma, -1);
				}
			);
		};

		new karma(self.Context, EventProcessor, dAmn);
		test.done();
	},

	testGiveKarmaItemExists: function (test) {
		var self = this;
		test.expect(5);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_givekarma'
			, {from: 'a', content: '!givekarma test'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM karma'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.item, 'test');
					test.strictEqual(row.karma, 666);
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO karma (item, karma, last_modified) "
			+ "VALUES ('test', 665, 1)"
			, function (err) {
				test.strictEqual(err, null);
				new karma(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testTakeKarmaItemExists: function (test) {
		var self = this;
		test.expect(5);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_takekarma'
			, {from: 'a', content: '!takekarma test'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.get(
				'SELECT * FROM karma'
				, function (err, row) {
					test.strictEqual(err, null);
					test.strictEqual(row.id, 1);
					test.strictEqual(row.item, 'test');
					test.strictEqual(row.karma, 664);
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO karma (item, karma, last_modified) "
			+ "VALUES ('test', 665, 1)"
			, function (err) {
				test.strictEqual(err, null);
				new karma(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testAddReasonItemNotExists: function (test) {
		var self = this;
		test.expect(7);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_givekarma'
			, {from: 'a', content: '!givekarma test for doing the testing'});

		var dAmn = {sendMsg: function () {}};
		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			if (values.$delta === undefined) {
				dbRun.call(self.Context.maindb, query, values, fn);
			} else {
				dbRun.call(self.Context.maindb, query, values, function (err) {
					test.strictEqual(err, null);
					self.Context.maindb.get(
						'SELECT * FROM karma_reasons'
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(row.id, 1);
							test.strictEqual(row.item_id, 1);
							test.strictEqual(row.delta, 1);
							test.strictEqual(row.user, 'a');
							test.strictEqual(
								row.reason
								, 'for doing the testing');
						}
					);
				});
			}
		};

		new karma(self.Context, EventProcessor, dAmn);
		test.done();
	},

	testAddReasonItemExists: function (test) {
		var self = this;
		test.expect(8);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_givekarma'
			, {from: 'a', content: '!givekarma test for doing the testing'});

		var dAmn = {sendMsg: function () {}};
		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			if (values.$delta === undefined) {
				dbRun.call(self.Context.maindb, query, values, fn);
			} else {
				dbRun.call(self.Context.maindb, query, values, function (err) {
					test.strictEqual(err, null);
					self.Context.maindb.get(
						'SELECT * FROM karma_reasons'
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(row.id, 1);
							test.strictEqual(row.item_id, 1);
							test.strictEqual(row.delta, 1);
							test.strictEqual(row.user, 'a');
							test.strictEqual(
								row.reason
								, 'for doing the testing');
						}
					);
				});
			}
		};

		dbRun.call(
			self.Context.maindb
			, "INSERT INTO karma (item, karma, last_modified) "
			+ "VALUES ('test', 665, 1)"
			, function (err) {
				test.strictEqual(err, null);
				new karma(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	}
};
