'use strict';

var choose = require('../listeners/cmd_choose.js');
var utils = require('../lib/test_utils.js');

exports.testInvalid = function (test) {
	test.expect(0);

	var inputs = ['!choose', '!choose ', '!choose A'];
	var Context = utils.getMockContext(this);
	Context.Packet = utils.getMockPacket();

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor = utils.getMockEventProcessor({content: input});
		new choose(Context, EventProcessor, dAmn);
	});

	test.done();
};

exports.testChoose = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor(
			{content: '!choose A or B or C or D or E', from: 'User'});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'User: D');
	};

	Context.MathsUtils.randomInt = function (min, max) {
		test.strictEqual(max, 4);
		return 3;
	};

	new choose(Context, EventProcessor, dAmn);
	test.done();
};
