'use strict';

var fmt = require('util').format;
var utils = require('../lib/test_utils.js');
var dAmn = require('../lib/damn.js');
var Packet = require('../lib/packet.js');

exports.testHandshakeNoConfigOrClient = function (test) {
	test.expect(2);

	var self = this;
	var Context = {};
	utils.interceptLogging(Context, self);
	var d = new dAmn(Context);

	d.handshake();
	test.strictEqual(
		self.error
		, 'No client or config data passed to login method.');

	Context.client = {};
	d.handshake();
	test.strictEqual(
		self.error
		, 'No client or config data passed to login method.');

	test.done();
};

exports.testSend = function (test) {
	test.expect(1);

	var Context = {
		client: {
			write: function (msg) {
				test.strictEqual(msg, 'msg\n\0');
			}
		}
	};

	var d = new dAmn(Context);
	d.send('msg');

	test.done();
};

exports.tests = {
	setUp: function (cb) {
		var self = this;
		var EventProcessor = {};

		self.Context = {cmdLine: {prettyLogs: false}};
		utils.interceptLogging(self.Context, self);

		self.Context.Packet = new Packet();

		self.Context.client = {
			write: function () {}
		};

		self.Context.config = {
			agentName: 'koybot'
			, name: 'bot'
		};

		self.Context.OAuth = {
			getAccessToken: function (clientID, clientSecret, cb) {
				cb('1234');
			}
		};

		self.Context.TUIBridge = {
			setup: function () {}
			, joinChannel: function () {}
		};

		self.d = new dAmn(self.Context, EventProcessor);
		self.error = null;
		self.log = null;
		cb();
	},

	testHandshakeSend: function (test) {
		test.expect(1);

		var self = this;
		self.d.send = function (packet) {
			test.strictEqual(
				packet
				, fmt('%s 0.3\nagent=koybot', self.d.Commands.DAMN_CLIENT));
		};

		self.d.handshake();

		test.done();
	},

	testSendPrivMsg: function (test) {
		test.expect(2);

		var self = this;
		var count = 0;
		self.d.sendGenericMsg = function (type, channel, msg) {
			count++;
			if (count === 1) {
				test.strictEqual(channel, 'a:bot');
			} else if (count === 2) {
				test.strictEqual(channel, 'bot:c');
			}
		};

		self.d.sendPrivMsg('a');
		self.d.sendPrivMsg('c');

		test.done();
	},

	testSendGenericMsg: function (test) {
		test.expect(1);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'send chat:chatroom\n\nmsg main\n\nstuff');
		};

		this.d.sendGenericMsg('chat', 'chatroom', 'stuff');

		test.done();
	},

	testSendGenericMsgChunked: function (test) {
		test.expect(3);

		var count = 0;
		this.d.send = function (msg) {
			var len = (count < 2) ? 8094 : 286;
			test.strictEqual(msg.length, len);
			count++;
		};

		// Generate a sufficiently long string to start getting chunked.
		var msg = '';
		var str =
			'\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000'
			+ '\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000';

		for (var i = 0; i < 1024; i++) {
			msg += str;
		}

		this.d.sendGenericMsg('chat', 'chatroom', msg);

		test.done();
	},

	testSendAction: function (test) {
		test.expect(1);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'send chat:chatroom\n\naction main\n\nstuff');
		};

		this.d.sendAction('chatroom', 'stuff');

		test.done();
	},

	testKick: function (test) {
		test.expect(2);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'kick chat:chan\nu=user');
		};

		this.d.kick('chan', 'user', null);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'kick chat:chan\nu=user\n\nraisin');
		};

		this.d.kick('chan', 'user', 'raisin');

		test.done();
	},

	testSetProperty: function (test) {
		test.expect(2);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'set chat:chan\np=topic\n\nvalue');
		};

		this.d.setProperty('chan', 'topic', 'value');

		this.d.send = function (msg) {
			test.strictEqual(msg.length, 8087);
		};

		var str = '00000000000000000000000000000000000';
		var value = '';

		for (var i = 0; i < 1024; i++) {
			value += str;
		}

		this.d.setProperty('chan', 'topic', value);

		test.done();
	},

	testSetTopic: function (test) {
		test.expect(1);

		this.d.send = function (msg) {
			test.strictEqual(msg, 'set chat:chan\np=topic\n\nvalue');
		};

		this.d.setTopic('chan', 'value');

		test.done();
	},

	testRecvInvalidPacket: function (test) {
		test.expect(1);

		this.d.recv('\n');
		test.strictEqual(this.error, 'Unable to parse packet.');

		test.done();
	},

	testRecvHandlesPing: function (test) {
		var self = this;
		test.expect(1);

		self.d.send = function (msg) {
			test.strictEqual(msg, self.d.Commands.PONG);
		};

		self.d.recv(fmt('%s\n', self.d.Commands.PING));

		test.done();
	},

	testHandshakeProtocolViolation: function (test) {
		test.expect(2);

		this.d.recv('test\n');
		test.strictEqual(this.error, 'Handshake protocol violation.');

		this.d.recv(fmt('%s 0.2\n', this.d.Commands.DAMN_SERVER));
		test.strictEqual(this.error, 'Handshake protocol violation.');

		test.done();
	},

	testLoginReceiveCorrectServerPacket: function (test) {
		test.expect(1);

		var self = this;
		self.d.send = function (msg) {
			test.strictEqual(
				msg
				, fmt('%s bot\npk=1234', self.d.Commands.LOGIN));
		};

		self.d.recv(fmt('%s 0.3\n', self.d.Commands.DAMN_SERVER));

		test.done();
	},

	testLoginProtocolViolation: function (test) {
		test.expect(3);

		this.d.recv(fmt('%s 0.3\n', this.d.Commands.DAMN_SERVER));

		this.d.recv('test\n');
		test.strictEqual(this.error, 'Login protocol violation.');

		this.d.recv(fmt('%s notbot\n', this.d.Commands.LOGIN));
		test.strictEqual(this.error, 'Login protocol violation.');

		this.d.recv(fmt('%s bot\n', this.d.Commands.LOGIN));
		test.strictEqual(this.error, 'Login protocol violation.');

		test.done();
	},

	testLoginUnsuccessful: function (test) {
		test.expect(1);

		this.d.recv(fmt('%s 0.3\n', this.d.Commands.DAMN_SERVER));
		this.d.recv(fmt('%s bot\ne=bad\n', this.d.Commands.LOGIN));
		test.strictEqual(this.error, 'Login unsuccessful: bad');

		test.done();
	},

	testNoChannelsToJoin: function (test) {
		test.expect(1);

		this.Context.config.channels = [];
		this.d.recv(fmt('%s 0.3\n', this.d.Commands.DAMN_SERVER));
		this.d.recv(fmt('%s bot\ne=ok\n', this.d.Commands.LOGIN));
		test.strictEqual(this.log, 'No channels to join.');

		test.done();
	},

	testJoinChannels: function (test) {
		test.expect(2);

		var self = this;
		var channels = ['a', 'b'];
		var count = 0;

		self.d.send = function (msg) {
			if (count < 1) {
				count++;
				return;
			}

			test.strictEqual(
				msg
				, fmt('%s chat:%s', self.d.Commands.JOIN, channels[count - 1]));
			count++;
		};

		self.Context.config.channels = channels;
		self.d.recv(fmt('%s 0.3\n', self.d.Commands.DAMN_SERVER));
		self.d.recv(fmt('%s bot\ne=ok\n', self.d.Commands.LOGIN));

		test.done();
	},

	testUnhandledPacket: function (test) {
		test.expect(1);

		this.Context.config.channels = ['chan'];
		this.d.recv(fmt('%s 0.3\n', this.d.Commands.DAMN_SERVER));
		this.d.recv(fmt('%s bot\ne=ok\n', this.d.Commands.LOGIN));
		this.d.recv(fmt('404\n'));
		test.strictEqual(this.log, 'Unhandled command: 404');

		test.done();
	},

	testHandleRecvPacket: function (test) {
		test.expect(1);

		var self = this;

		self.Context.config.channels = ['a'];
		self.Context.Recv = {
			handle: function (packet) {
				test.deepEqual(
					packet
					, {
						cmd: self.d.Commands.RECV
						, param: undefined
						, args: {}
						, body: ''
					}
				);
			}
		};

		self.d.recv(fmt('%s 0.3\n', self.d.Commands.DAMN_SERVER));
		self.d.recv(fmt('%s bot\ne=ok\n', self.d.Commands.LOGIN));
		self.d.recv(fmt('%s\n', self.d.Commands.RECV));

		test.done();
	},

	testHandlePropertyPacket: function (test) {
		test.expect(1);

		var self = this;

		self.Context.config.channels = ['a'];
		self.Context.Property = {};
		self.Context.Property.handle = function (packet) {
			test.deepEqual(
				packet
				, {
					cmd: self.d.Commands.PROPERTY
					, param: 'chat:chan'
					, args: {p: 'topic'}
					, body: 'value\n'
				});
		};

		self.d.recv(fmt('%s 0.3\n', self.d.Commands.DAMN_SERVER));
		self.d.recv(fmt('%s bot\ne=ok\n', self.d.Commands.LOGIN));
		self.d.recv(fmt(
			'%s chat:chan\np=topic\n\nvalue\n'
			, self.d.Commands.PROPERTY));

		test.done();
	}
};
