'use strict';

var define = require('../listeners/cmd_define.js');
var utils = require('../lib/test_utils.js');

var getMockContext = function (attach) {
	var Context = utils.getMockContext(attach);

	Context.config = {
		dictionaryKey: 'dict-key'
	};

	Context.http = {};
	Context.http.get = function (url, fn) {
		fn({
			on: function (evt, evtfn) {
				if (evt === 'data') {
					evtfn('<xml>');
				} else if (evt === 'end') {
					evtfn();
				}
			}
		});

		return {on: function () {}};
	};

	return Context;
};

exports.testDisabled = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define', from: 'a'});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a: That command is currently disabled.');
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};

exports.testNoParameters = function (test) {
	test.expect(0);

	var Context = getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!define'});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};

exports.testLookupError = function (test) {
	test.expect(3);

	var Context = getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({content: '!define TEST'});

	Context.http = {};
	Context.http.get = function (url, fn) {
		test.strictEqual(
			url
			, 'http://www.dictionaryapi.com/api/v1/references/collegiate/xml/'
			+ 'test?key=dict-key');

		var req = {};
		req.on = function (evt, evtfn) {
			test.strictEqual(evt, 'error');
			evtfn({message: 'bad'});
		};

		return req;
	};

	new define(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'There was an error looking up the definition of "TEST": bad');

	test.done();
};

exports.testInvalidXML = function (test) {
	test.expect(3);

	var dAmn = {};
	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define test', from: 'a'});

	Context.http = {};
	Context.http.get = function (url, fn) {
		fn({
			on: function (evt, evtfn) {
				if (evt === 'data') {
					evtfn('<xml>');
					evtfn('</xml>');
				} else if (evt === 'end') {
					evtfn();
				}
			}
		});

		return {on: function () {}};
	};

	Context.xml = {};
	Context.xml.XmlDocument = function (xml) {
		test.strictEqual(xml, '<xml></xml>');
		return {};
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: No definitions were found which '
			+ 'matched your search parameters.');
	};

	new define(Context, EventProcessor, dAmn);
	test.strictEqual(
		this.error
		, 'Invalid XML received when looking up "test".');

	test.done();
};

exports.testSendSuggestion = function (test) {
	test.expect(1);

	var Context = getMockContext();
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define test', from: 'a'});

	Context.xml = {};
	Context.xml.XmlDocument = function (xml) {
		return {
			childNamed: function (name) {
				return {val: 'text'};
			}
		};
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a: Did you mean <b><i>text</i></b>?');
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};

exports.testNoFormsFound = function (test) {
	test.expect(2);

	var dAmn = {};
	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define test', from: 'a'});

	Context.xml = {};
	Context.xml.XmlDocument = function (xml) {
		return {
			childNamed: function (name) {}
			, children: []
		};
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: No definitions were found which '
			+ 'matched your search parameters.');
	};

	new define(Context, EventProcessor, dAmn);
	test.strictEqual(
		this.error
		, 'Valid XML was received but seemed to contain no '
		+ 'dictionary data when looking up "test".');

	test.done();
};

exports.testFormNotFound = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define test noun', from: 'a'});

	Context.xml = {};
	Context.xml.XmlDocument = function (xml) {
		return {
			childNamed: function (name) {}
			, children: [{
				childNamed: function (name) {
					if (name === 'ew') {
						return {val: 'test'};
					} else {
						return {val: 'verb'};
					}
				}
			}]
		};
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: No definitions were found which '
			+ 'matched your search parameters.');
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};

exports.testDefinitionsNotFound = function (test) {
	test.expect(1);

	var Context = getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!define test', from: 'a'});

	Context.ent = {};
	Context.ent.encode = function () {
		return '';
	};

	Context.xml = {};
	Context.xml.XmlDocument = function (xml) {
		return {
			childNamed: function (name) {}
			, children: [{
				childNamed: function (name) {
					if (name === 'ew') {
						return {val: 'test'};
					} else if (name === 'fl') {
						return {val: 'noun'};
					}
				},

				childrenNamed: function (name) {
					return [];
				}
			}]
		};
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: No definitions were found which '
			+ 'matched your search parameters.');
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};

exports.testSuccess = function (test) {
	test.expect(4);

	var Context = getMockContext(this);
	var EventProcessor = utils.getMockEventProcessor({
		content: '!define test'
		, from: 'a'
		, channel: 'chan'});

	var fs = require('fs');
	var data = fs.readFileSync('tests/resources/dict-test.xml');

	Context.ent = require('ent');
	Context.xml = require('xmldoc');
	Context.http.get = function (url, fn) {
		fn({
			on: function (evt, evtfn) {
				if (evt === 'data') {
					evtfn(data);
				} else if (evt === 'end') {
					evtfn();
				}
			}
		});

		return {on: function () {}};
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(
			msg
			, '2 nouns, 1 adjective and 1 verb found.<br />'
			+ 'test, <i>noun</i> /&#712;test/<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 1. cupel<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 2. a critical examination, '
			+ 'observation, or evaluation; trial<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 3. the procedure of submitting a '
			+ 'statement to such conditions or operations as will lead to its '
			+ 'proof or disproof or to its acceptance or rejection <br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 4. a basis for evaluation; '
			+ 'criterion<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 5. an ordeal or oath required as proof '
			+ 'of conformity with a set of beliefs<br />');
	};

	new define(Context, EventProcessor, dAmn);

	Context.config.maxDefinitions = 1;
	EventProcessor = utils.getMockEventProcessor({
		content: '!define test adjective full'
		, from: 'a'
		, channel: 'chan'
	});

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, '2 nouns, 1 adjective and 1 verb found.<br />'
			+ 'test, <i>adjective</i> /&#712;test/<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 1. of, relating to, '
			+ 'or constituting a test<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 2. subjected to, used for, '
			+ 'or revealed by   testing<br />');
	};

	new define(Context, EventProcessor, dAmn);

	EventProcessor = utils.getMockEventProcessor({
		content: '!define test noun #2 full'
		, from: 'a'
		, channel: 'chan'
	});

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, '2 nouns, 1 adjective and 1 verb found.<br />'
			+ 'test, <i>noun</i> /&#712;test/<br />'
			+ '&nbsp;&nbsp;&nbsp;&nbsp; 1. an external hard or firm covering '
			+ '(as a shell) of many invertebrates '
			+ '(as a foraminifer or a mollusk)<br />');
	};

	new define(Context, EventProcessor, dAmn);
	test.done();
};
