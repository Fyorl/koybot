'use strict';

var weather = require('../listeners/cmd_weather.js');
var utils = require('../lib/test_utils.js');

exports.testInvalid = function (test) {
	test.expect(0);

	var inputs = ['!weather', '!weather '];
	var Context = utils.getMockContext(this);
	Context.Packet = utils.getMockPacket();

	Context.http = {};
	Context.http.get = function (url, fn) {
		test.ok(false);
	};

	inputs.forEach(function (input) {
		var EventProcessor = utils.getMockEventProcessor({content: input});
		new weather(Context, EventProcessor);
	});

	test.done();
};

exports.testGetWeatherError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!weather city'});

	Context.http = {};
	Context.http.get = function (url, fn) {
		return {
			on: function (evt, evtfn) {
				evtfn({message: 'bad'});
			}
		};
	};

	new weather(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error retrieving weather information for "city": bad');

	test.done();
};

exports.testInvalidJSON = function (test) {
	test.expect(2);

	var dAmn = {};
	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!weather city'});

	Context.http = {};
	Context.http.get = function (url, fn) {
		fn({
			on: function (evt, evtfn) {
				if (evt === 'data') {
					evtfn('{');
				} else {
					evtfn();
				}
			}
		});

		return {on: function () {}};
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'Bad response from weather server, please try again.');
	};

	new weather(Context, EventProcessor, dAmn);
	test.strictEqual(
		this.error
		, 'Unable to parse response "{" when checking weather: '
		+ 'Unexpected end of input');

	test.done();
};

exports.testInvalidResponseObject = function (test) {
	var self = this;
	var inputs = [
		{}
		, {}
		, {sys: {}}
		, {name: '', sys: {country: ''}, weather: []}
		, {name: '', sys: {country: ''}, weather: [{}]}
		, {name: '', sys: {country: ''}, weather: [{description: ''}]}
		, {
			name: ''
			, sys: {country: ''}
			, weather: [{description: ''}]
			, main: {}
		}, {
			name: ''
			, sys: {country: ''}
			, weather: [{description: ''}]
			, main: {temp: ''}
		}, {
			message: 'Error: Not found city'
			, cod: 404
		}
	];

	test.expect(inputs.length + 1);
	var dAmn = {};
	var Context = utils.getMockContext(self);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!weather city'});

	var getHttp = function (contents) {
		return function (url, fn) {
			fn({
				on: function (evt, evtfn) {
					if (evt === 'data') {
						evtfn(JSON.stringify(contents));
					} else {
						evtfn();
					}
				}
			});

			return {on: function () {}};
		};
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'Error: Not found city');
	};

	inputs.forEach(function (input) {
		Context.http = {};
		Context.http.get = getHttp(input);

		new weather(Context, EventProcessor, dAmn);
		test.strictEqual(
			self.error
			, Context.fmt(
				'Corrupt response when retrieving weather information: %j'
				, input));
	});

	test.done();
};

exports.testWeatherSuccess = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!weather city', from: 'a'});

	var responseObject = {
		name: 'City'
		, sys: {country: 'XX'}
		, weather: [{description: 'very heavy rain'}]
		, main: {temp: 42, humidity: 99}
	};

	Context.http = {};
	Context.http.get = function (url, fn) {
		fn({
			on: function (evt, evtfn) {
				if (evt === 'data') {
					evtfn('');
					evtfn(JSON.stringify(responseObject));
				} else {
					evtfn();
				}
			}
		});

		return {on: function () {}};
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: City, XX, 42&#176;c, 99%, <i>very heavy rain</i>');
	};

	new weather(Context, EventProcessor, dAmn);
	test.done();
};
