'use strict';

var tablumps = require('../lib/tablumps.js');
var testUtils = require('../lib/test_utils.js');

exports.testBadTablump = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var converter = new tablumps(Context);

	converter.convert('&404\t');
	test.strictEqual(this.error, 'Encountered bad tablump: 404');
	test.done();
};

exports.testValid = function (test) {
	var tests = [
		['&a\turl\ttitle\ttext&/a\t'
		, '<a href="url" title="title">text</a>']
		, ['&b\tbold&/b\t'
		, '<strong>bold</strong>']
		, ['&sub\tso small&/sub\t'
		, '<sub>so small</sub>']
		, ['&abbr\ttitle\tabbreviation&/abbr\t'
		, '<abbr title="title">abbreviation</abbr>']
		, ['&dev\t~\tfyorl\t'
		, ':devfyorl:']
		, ['&avatar\trubcheeksplz\t9\t'
		, ':iconrubcheeksplz:']
		, ['&img\tsrc\th\tw\t'
		, '<img src="src" height="h" width="w" />']
		, ['&emote\t:)\t15\t15\t:)\t???\t'
		, ':)']
		, ['&thumb\t01189998819991197253\t???\t???\t???\t???\t???\t'
		, ':thumb01189998819991197253:']
	];

	test.expect(tests.length);

	var Context = testUtils.getMockContext(this);
	var converter = new tablumps(Context);

	tests.forEach(function (tuple) {
		test.strictEqual(converter.convert(tuple[0]), tuple[1]);
	});

	test.done();
};

exports.testLink = function (test) {
	var tests = [
		['&link\turl\t&\t'
		, '<a href="url" title="url">[link]</a>']
		, ['&link\turl\ttext\t&\t'
		, '<a href="url" title="url">text</a>']
	];

	test.expect(tests.length);

	var Context = testUtils.getMockContext(this);
	var converter = new tablumps(Context);

	tests.forEach(function (tuple) {
		test.strictEqual(converter.convert(tuple[0]), tuple[1]);
	});

	test.done();
};

exports.testPrematureTermination = function (test) {
	var tests = [
		['&emote\t:(\t15\t15\t:('
		, ':(']
		, ['&emote\t:la:\t19\t19\tla'
		, ':la:']
		, ['&avatar\trubcheeksplz'
		, ':iconrubcheeksplz:']
		, ['&b\tbold&/b'
		, '<strong>bold</strong>']
		, ['&abbr'
		, '<abbr title="">']
		, ['&abbr\t'
		, '<abbr title="">']
	];

	test.expect(tests.length);

	var Context = testUtils.getMockContext(this);
	var converter = new tablumps(Context);

	tests.forEach(function (tuple) {
		test.strictEqual(converter.convert(tuple[0]), tuple[1]);
	});

	test.done();
};

