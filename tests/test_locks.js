'use strict';

var Locks = require('../lib/locks.js');
var utils = require('../lib/test_utils.js');

exports.testLocks = function (test) {
	test.expect(3);

	var Context = utils.getMockContext(this);
	var locks = new Locks(Context);

	Context.cmdLine = {debug: true};

	var lock1 = locks.create();
	var lock2 = locks.create();

	lock2.acquire(function () {});
	test.strictEqual(this.log, 'Acquiring lock #2...');
	lock2.release();
	test.strictEqual(this.log, 'Releasing lock #2...');

	var release = function () {
		lock1.release();
	};

	lock1.acquire(
		function () {
			setTimeout(release, 400);
		}
	);

	var start = new Date().getTime();
	var partTwo = function () {
		var end = new Date().getTime();
		test.ok((end - start) >= 400);
		lock1.release();
		lock1.acquire(endTest);
	};

	lock1.acquire(partTwo);

	var endTest = function () {
		lock1.release();
		test.done();
	};
};
