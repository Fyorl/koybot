'use strict';

require('../lib/function.js');
var economy = require('../listeners/word_economy.js');
var testUtils = require('../lib/test_utils.js');
var dbUtils = require('../lib/db_utils.js');

exports.testPenceToPoundsValid = function (test) {
	test.expect(4);

	var inputs = [9, 10, 200, 17984501];
	var outputs = [
		'&pound;0.09'
		, '&pound;0.10'
		, '&pound;2.00'
		, '&pound;179,845.01'
	];

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_balance'
		, {content: '!balance', from: 'user'});

	var get = function (balance) {
		return function (query, values, fn) {
			fn(null, {balance: balance});
		};
	};

	var sendMsg = function (output) {
		return function (channel, msg) {
			test.strictEqual(msg, Context.fmt('user: %s', output));
		};
	};

	for (var i = 0; i < inputs.length; i++) {
		Context.maindb.get = get(inputs[i]);
		dAmn.sendMsg = sendMsg(outputs[i]);
		new economy(Context, EventProcessor, dAmn);
	}

	test.done();
};

exports.testCmdBalanceInvalid = function (test) {
	test.expect(2);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_balance'
		, {content: '!balance', from: 'user'});

	Context.maindb.get = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error trying to retrieve balance for user "user": bad');

	Context.maindb.get = function (query, values, fn) {
		fn(null, {});
	};

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Invalid row returned when retrieving balance for user "user".');

	test.done();
};

exports.testCmdBalanceValid = function (test) {
	test.expect(2);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_balance'
		, {content: '!balance', from: 'user'});

	Context.maindb.get = function (query, values, fn) {
		fn();
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'user: &pound;0.00');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		fn(null, {balance: 99});
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'user: &pound;0.99');
	};

	new economy(Context, EventProcessor, dAmn);

	test.done();
};

exports.testCmdWordPriceInvalid = function (test) {
	test.expect(0);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_wordprice'
		, {content: '!wordprice ', from: 'user'});

	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	new economy(Context, EventProcessor, dAmn);

	test.done();
};

exports.testCmdWordPriceError = function (test) {
	test.expect(1);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_wordprice'
		, {content: '!wordprice a', from: 'user'});

	Context.maindb.get = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(this.error, 'Error checking word "a" ownership: bad');
	test.done();
};

exports.testCmdWordPriceValid = function (test) {
	test.expect(4);

	var inputs = ['a', 'and', 'trousers', 'antidisestablishmentarianism'];
	var outputs = [
		'&pound;40.74'
		, '&pound;26.11'
		, '&pound;6.49'
		, '&pound;0.01'
	];

	var dAmn = {};
	var Context = testUtils.getMockContext(this);

	Context.maindb.get = function (query, values, fn) {
		fn(null, null);
	};

	var sendMsg = function (input, output) {
		return function (channel, msg) {
			test.strictEqual(
				msg
				, Context.fmt(
					'user: "%s" will cost %s to buy.'
					, input
					, output));
		};
	};

	for (var i = 0; i < inputs.length; i++) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_wordprice'
			, {content: '!wordprice ' + inputs[i], from: 'user'});

		dAmn.sendMsg = sendMsg(inputs[i], outputs[i]);
		new economy(Context, EventProcessor, dAmn);
	}

	test.done();
};

exports.testCmdGiveInvalid = function (test) {
	test.expect(0);

	var inputs = ['x', 'user \u00A31.00', 'x fishsticks', 'x 0.00999'];
	var Packet = require('../lib/packet.js');

	var Context = testUtils.getMockContext(this);
	Context.Packet = new Packet(Context);
	Context.maindb.all = function (query, values, fn) {
		test.ok(false);
	};

	for (var i = 0; i < inputs.length; i++) {
		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_give'
			, {content: '!give ' + inputs[i], from: 'user'});

		new economy(Context, EventProcessor);
	}

	test.done();
};

exports.testCmdGiveError = function (test) {
	test.expect(3);

	var dAmn = {};
	var Packet = require('../lib/packet.js');
	var Context = testUtils.getMockContext(this);
	Context.Packet = new Packet(Context);

	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_give'
		, {content: '!give x 1', from: 'user'});

	Context.maindb.all = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error checking balances for players user and x: bad');

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_give'
		, {content: '!give x 0.02', from: 'user'});

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{user: 'user', balance: 1}]);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'user: Insufficient funds.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{user: 'user', balance: 100}]);
	};

	Context.maindb.run = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error updating balances for "user" and "x": bad');

	test.done();
};

exports.testCmdGiveValid = function (test) {
	test.expect(5);

	var dAmn = {};
	var Packet = require('../lib/packet.js');
	var Context = testUtils.getMockContext(this);
	Context.Packet = new Packet(Context);

	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_give'
		, {content: '!give x 0.1', from: 'user'});

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{user: 'user', balance: 100}, {user: 'x', balance: 1}]);
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(values.$frombalance, 90);
		test.strictEqual(values.$tobalance, 11);
		fn();
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'user: Gave &pound;0.10 to x.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{user: 'user', balance: 100}]);
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(values.$frombalance, 90);
		test.strictEqual(values.$tobalance, 10);
	};

	new economy(Context, EventProcessor);

	test.done();
};

exports.testCmdBuyWordInvalid = function (test) {
	test.expect(0);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buyword'
		, {content: '!buyword', from: 'user'});

	Context.maindb.get = function (query, values, fn) {
		test.ok(false);
	};

	new economy(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buyword'
		, {content: '!buyword ', from: 'user'});

	new economy(Context, EventProcessor);

	test.done();
};

exports.testCmdBuyWordError = function (test) {
	test.expect(3);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buyword'
		, {content: '!buyword and', from: 'user'});

	Context.maindb.get = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error checking if word "and" already bought: bad');

	Context.maindb.get = function (query, values, fn) {
		if (values.$word) {
			fn();
		} else if (values.$user) {
			fn(null, {balance: 100000});
		}
	};

	Context.maindb.run = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error updating balance for "user" to 97389: bad');

	Context.maindb.run = function (query, values, fn) {
		if (values.$balance) {
			fn();
		} else if (values.$word) {
			fn({message: 'bad'});
		}
	};

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Error adding "and" to word list of user "user": bad');

	test.done();
};

exports.testCmdBuyWord = function (test) {
	test.expect(3);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buyword'
		, {content: '!buyword and', from: 'user'});

	Context.maindb.get = function (query, values, fn) {
		fn(null, {user: 'x'});
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'user: "and" is already owned by x.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		if (values.$word) {
			fn();
		} else if (values.$user) {
			fn(null, {balance: 0});
		}
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'user: Insufficient funds. "and" costs &pound;26.11, '
			+ 'you have &pound;0.00.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		if (values.$word) {
			fn();
		} else if (values.$user) {
			fn(null, {balance: 3000});
		}
	};

	Context.maindb.run = function (query, values, fn) {
		fn();
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'user: Purchased "and" for &pound;26.11. '
			+ 'Remaining balance is &pound;3.89.');
	};

	new economy(Context, EventProcessor, dAmn);

	test.done();
};

exports.testIgnoreBotLines = function (test) {
	test.expect(0);

	var Context = testUtils.getMockContext(this);
	Context.config = {name: 'KoYbot'};

	var EventProcessor = testUtils.getMockEventProcessor(
		'recvMsg'
		, {content: 'Hello, user!', from: 'KoYbot'});

	Context.maindb.all = function (query, values, fn) {
		test.ok(false);
	};

	test.done();
};

exports.testCmdMyWords = function (test) {
	test.expect(4);

	var dAmn = {};
	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_mywords'
		, {content: '!mywords', from: 'a'});

	Context.maindb.all = testUtils.injectError();

	new economy(Context, EventProcessor);
	test.strictEqual(this.error, 'Error checking words owned by "a": bad');

	Context.maindb.all = function (query, values, fn) {
		fn(null, []);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a: You own no words.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{word: '&emote\t:)\t15\t15\t:)'}]);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a, you own the following word: ":)"');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.all = function (query, values, fn) {
		fn(null, [{word: 'and'}, {word: 'the'}]);
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(msg, 'a, you own the following words: "and", "the"');
	};

	new economy(Context, EventProcessor, dAmn);

	test.done();
};

exports.testBuyKick = function (test) {
	test.expect(8);

	var Context = testUtils.getMockContext(this);
	Context.config = {};
	Context.config.name = 'KoYbot';

	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buykick'
		, {content: '!buykick', from: 'a'});

	Context.maindb.get = function (query, values, fn) {
		test.ok(false);
	};

	new economy(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buykick'
		, {content: '!buykick koybot', from: 'a'});

	new economy(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buykick'
		, {content: '!buykick b for sucking hard', from: 'a', channel: 'chan'});

	Context.maindb.get = function (query, values, fn) {
		if (values.$word == null) {
			fn(null, null);
		} else {
			fn({message: 'bad'});
		}
	};

	new economy(Context, EventProcessor);
	test.strictEqual(this.error, 'Error checking if "b" was owned by "a": bad');

	Context.maindb.get = function (query, values, fn) {
		if (values.$word == null) {
			fn(null, null);
		} else {
			fn(null, null);
		}
	};

	new economy(Context, EventProcessor);
	test.strictEqual(
		this.error
		, 'Invalid row returned when checking if "b" was owned by "a": null');

	Context.maindb.get = function (query, values, fn) {
		if (values.$word == null) {
			fn(null, null);
		} else {
			fn(null, {'count(*)': 0});
		}
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: Insufficient funds. Kicking b will cost &pound;2.00.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		if (values.$word == null) {
			fn(null, null);
		} else {
			fn(null, {'count(*)': 1});
		}
	};

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: Insufficient funds. Kicking b will cost &pound;1.50.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		if (values.$word == null) {
			fn(null, {balance: 204});
		} else {
			fn(null, {'count(*)': 0});
		}
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(values.$balance, 4);
		fn();
	};

	dAmn.kick = function (channel, target, reason) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(target, 'b');
		test.strictEqual(reason, 'for sucking hard');
	};

	new economy(Context, EventProcessor, dAmn);

	test.done();
};

exports.testBuyTopic = function (test) {
	test.expect(6);

	var Context = testUtils.getMockContext(this);
	var EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buytopic'
		, {content: '!buytopic', from: 'a'});

	Context.maindb.get = function (query, values, fn) {
		test.ok(false);
	};

	new economy(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buytopic'
		, {content: '!buytopic ', from: 'a'});

	new economy(Context, EventProcessor);

	EventProcessor = testUtils.getMockEventProcessor(
		'cmd_buytopic'
		, {
			content: '!buytopic new topic &emote\t:)\t15\t15\t:)'
			, from: 'a'
			, channel: 'chan'
		}
	);

	Context.maindb.get = function (query, values, fn) {
		fn(null, null);
	};

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'a: Insufficient funds. '
			+ 'Purchasing a new topic costs &pound;5.00.');
	};

	new economy(Context, EventProcessor, dAmn);

	Context.maindb.get = function (query, values, fn) {
		fn(null, {balance: 1000});
	};

	Context.maindb.run = function (query, values, fn) {
		test.strictEqual(values.$balance, 500);
		fn();
	};

	var Packet = require('../lib/packet.js');
	Context.Packet = new Packet(Context);
	Context.memory = {};
	Context.KeyValueStore = {put: function(){}};

	var DAmn = require('../lib/damn.js');
	dAmn = new DAmn(Context);

	dAmn.send = function (msg) {
		test.strictEqual(msg, 'set chat:chan\np=topic\n\nnew topic :)');
	};

	new economy(Context, EventProcessor, dAmn);

	dAmn.send = function () {};

	Context.maindb.run = function (query, value, fn) {
		fn();
	};

	Context.KeyValueStore.put = function (key, value, cb) {
		test.strictEqual(key, 'LastTopicChange');
		test.strictEqual(value.by, 'a');
	};

	new economy(Context, EventProcessor, dAmn);
	test.strictEqual(Context.memory.lastTopicChange.by, 'a');

	test.done();
};

exports.integrationTests = {
	setUp: function (cb) {
		var self = this;
		var Log = require('../lib/log.js');
		var Locks = require('../lib/locks.js');
		var Packet = require('../lib/packet.js');
		var KeyValueStore = require('../lib/key_value_store.js');
		var DatabaseLoader = require('../lib/database_loader.js');
		var Tablumps = require('../lib/tablumps.js');

		self.Context = {};
		self.Context.memory = {};
		self.Context.HelpText = {};
		self.Context.cmdLine = {debug: true};
		self.Context.config = {name: 'KoYbot'};
		self.Context.moment = require('moment');
		self.Context.sqlite3 = require('sqlite3').verbose();
		self.Context.fmt = require('util').format;
		self.Context.fs = require('fs');
		self.Context.Log = new Log();
		self.Context.Packet = new Packet(self.Context);
		self.Context.Locks = testUtils.getMockLocks();
		self.Context.DatabaseLoader = new DatabaseLoader(self.Context);
		self.Context.Tablumps = new Tablumps(self.Context);

		dbUtils.setupDBs(self.Context, function () {
			self.Context.KeyValueStore = new KeyValueStore(self.Context);
			cb();
		});
	},

	tearDown: function (cb) {
		dbUtils.cleanupDBs(this.Context, cb);
	},

	testCmdBalanceUserNotFound: function (test) {
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_balance'
			, {content: '!balance', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: &pound;0.00');
		};

		new economy(this.Context, EventProcessor, dAmn);

		test.done();
	},

	testCmdBalanceUserExists: function (test) {
		var self = this;
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_balance'
			, {content: '!balance', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: &pound;0.99');
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) VALUES('a', 99)"
			, function (err) {
				new economy(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testCmdGiveNeitherUserExists: function (test) {
		var self = this;
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_give'
			, {content: '!give b \u00A30.99', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: Insufficient funds.');
		};

		new economy(self.Context, EventProcessor, dAmn);

		test.done();
	},

	testCmdGiveGiverExists: function (test) {
		var self = this;
		test.expect(4);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_give'
			, {content: '!give b \u00A30.99', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: Insufficient funds.');
			giveSufficientBalance();
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) VALUES('a', 10)"
			, function (err) {
				new economy(self.Context, EventProcessor, dAmn);
			}
		);

		var giveSufficientBalance = function () {
			self.Context.maindb.run(
				"UPDATE word_economy_players SET balance = 100 WHERE id = 1"
				, function (err) {
					dAmn.sendMsg = function (channel, msg) {
						test.strictEqual(msg, 'a: Gave &pound;0.99 to b.');
						checkUpdatedBalances();
					};

					new economy(self.Context, EventProcessor, dAmn);
				}
			);
		};

		var checkUpdatedBalances = function () {
			self.Context.maindb.all(
				"SELECT * FROM word_economy_players ORDER BY id ASC"
				, function (err, rows) {
					test.deepEqual(rows[0], {id: 1, user: 'a', balance: 1});
					test.deepEqual(rows[1], {id: 2, user: 'b', balance: 99});
				}
			);
		};

		test.done();
	},

	testCmdGiveBothExist: function (test) {
		var self = this;
		test.expect(3);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_give'
			, {content: '!give b \u00A30.99', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: Gave &pound;0.99 to b.');
			checkUpdatedBalances();
		};

		var checkUpdatedBalances = function () {
			self.Context.maindb.all(
				"SELECT * FROM word_economy_players ORDER BY id ASC"
				, function (err, rows) {
					test.deepEqual(rows[0], {id: 1, user: 'a', balance: 1});
					test.deepEqual(rows[1], {id: 2, user: 'b', balance: 109});
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) "
			+ "VALUES('a', 100), ('b', 10)"
			, function (err) {
				new economy(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testCmdBuyWord: function (test) {
		var self = this;
		test.expect(5);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_buyword'
			, {content: '!buyword and', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(msg, 'a: "and" is already owned by b.');
			testInsufficientBalance();
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) "
			+ "VALUES('a', 1), ('b', 1)"
			, function (err) {
				self.Context.maindb.run(
					"INSERT INTO word_economy_words (user_id, word) "
					+ "VALUES(2, 'and')"
					, function (err) {
						new economy(self.Context, EventProcessor, dAmn);
					}
				);
			}
		);

		var testInsufficientBalance = function () {
			dAmn.sendMsg = function (channel, msg) {
				test.strictEqual(
					msg
					, 'a: Insufficient funds. "and" costs &pound;26.11, '
					+ 'you have &pound;0.01.');

				testBuyWord();
			};

			self.Context.maindb.run(
				'DELETE FROM word_economy_words'
				, function (err) {
					new economy(self.Context, EventProcessor, dAmn);
				}
			);
		};

		var testBuyWord = function () {
			dAmn.sendMsg = function (channel, msg) {
				test.strictEqual(
					msg
					, 'a: Purchased "and" for &pound;26.11. '
					+ 'Remaining balance is &pound;3.89.');

				checkDatabaseState();
			};

			self.Context.maindb.run(
				'UPDATE word_economy_players SET balance = 3000 WHERE id = 1'
				, function (err) {
					new economy(self.Context, EventProcessor, dAmn);
				}
			);
		};

		var checkDatabaseState = function () {
			self.Context.maindb.all(
				"SELECT players.id, players.user, players.balance, words.word "
				+ "FROM word_economy_players AS players, "
				+ "word_economy_words AS words "
				+ "WHERE words.user_id = players.id"
				, function (err, rows) {
					test.strictEqual(rows.length, 1);
					test.deepEqual(
						rows[0]
						, {id: 1, user: 'a', balance: 389, word: 'and'});
				}
			);
		};

		test.done();
	},

	testRecvMsgNoTax: function (test) {
		var self = this;
		test.expect(3);

		var inputs = [
			'hah!'
			, 'please don\'t, tax me on this'
			, 'this is a really long; line that attempts. to '
			+ 'far exceed: the maximum line limit'
		];

		var outputs = [2, 8, 18];

		var input;
		var output;

		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			dbRun.apply(
				self.Context.maindb
				, [query, values, checkUpdatedBalance]);
		};

		var checkUpdatedBalance = function () {
			self.Context.maindb.get(
				'SELECT * FROM word_economy_players'
				, function (err, row) {
					test.deepEqual(row, {id: 1, user: 'a', balance: output});
					doTest();
				}
			);
		};

		var doTest = function () {
			input = inputs.shift();
			output = outputs.shift();

			if (input == null) {
				return;
			}

			var EventProcessor = testUtils.getMockEventProcessor(
				'recvMsg'
				, {content: input, from: 'a'});

			new economy(self.Context, EventProcessor);
		};

		doTest();

		test.done();
	},

	testRecvMsgTax: function (test) {
		var self = this;
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'recvMsg'
			, {content: 'word1 word2', from: 'a'});

		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			dbRun.apply(
				self.Context.maindb
				, [query, values, checkUpdatedBalances]);
		};

		var checkUpdatedBalances = function () {
			self.Context.maindb.all(
				'SELECT * FROM word_economy_players ORDER BY id ASC'
				, function (err, rows) {
					test.deepEqual(rows, [
						{id: 1, user: 'b', balance: 20}
						, {id: 2, user: 'c', balance: 2}
						, {id: 3, user: 'a', balance: 2}
					]);
				}
			);
		};

		dbRun.apply(
			self.Context.maindb
			, [
				"INSERT INTO word_economy_players (user, balance) "
				+ "VALUES('b', 19), ('c', 1)"
				, function (err) {
					dbRun.apply(
						self.Context.maindb
						, [
							"INSERT INTO word_economy_words (user_id, word) "
							+ "VALUES(1, 'word1'), (2, 'word2')"
							, function (err) {
								new economy(self.Context, EventProcessor);
							}
						]
					);
				}
			]
		);

		test.done();
	},

	testRecvMsgSelfTax: function (test) {
		var self = this;
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'recvMsg'
			, {content: 'w1 w2 w3 w4 w5 w6 w7', from: 'a'});

		var dbRun = self.Context.maindb.run;
		self.Context.maindb.run = function (query, values, fn) {
			dbRun.apply(
				self.Context.maindb
				, [query, values, checkUpdatedBalances]);
		};

		var checkUpdatedBalances = function () {
			self.Context.maindb.all(
				'SELECT * FROM word_economy_players ORDER BY id ASC'
				, function (err, rows) {
					test.deepEqual(rows, [
						{id: 1, user: 'a', balance: 13}
						, {id: 2, user: 'b', balance: 26}
					]);
				}
			);
		};

		dbRun.apply(
			self.Context.maindb
			, [
				"INSERT INTO word_economy_players (user, balance) "
				+ "VALUES('a', 10), ('b', 22)"
				, function (err) {
					dbRun.apply(
						self.Context.maindb
						, [
							"INSERT INTO word_economy_words (user_id, word) "
							+ "VALUES(1, 'w7'), (2, 'w5'), (2, 'w2')"
							, function (err) {
								new economy(self.Context, EventProcessor);
							}
						]
					);
				}
			]
		);

		test.done();
	},

	testCmdMyWords: function (test) {
		var self = this;
		test.expect(1);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_mywords'
			, {content: '!mywords', from: 'A'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(
				msg
				, 'A, you own the following words: "and", "the"');
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) VALUES ('a', 0)"
			, function (err) {
				self.Context.maindb.run(
					"INSERT INTO word_economy_words (user_id, word) "
					+ "VALUES (1, 'and'), (1, 'the')"
					, function (err) {
						new economy(self.Context, EventProcessor, dAmn);
					}
				);
			}
		);

		test.done();
	},

	testCmdBuyKick: function (test) {
		var self = this;
		test.expect(2);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_buykick'
			, {content: '!buykick b', from: 'a'});

		var dAmn = {};
		dAmn.kick = function (channel, target, reason) {
			test.strictEqual(reason, null);
			self.Context.maindb.get(
				"SELECT balance FROM word_economy_players WHERE user = 'a'"
				, function (err, row) {
					test.deepEqual(row, {balance: 57});
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) VALUES ('a', 207)"
			, function (err) {
				self.Context.maindb.run(
					"INSERT INTO word_economy_words (user_id, word) "
					+ "VALUES (1, 'b')"
					, function (err) {
						new economy(self.Context, EventProcessor, dAmn);
					}
				);
			}
		);

		test.done();
	},

	testCmdBuyTopic: function (test) {
		var self = this;
		test.expect(10);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_buytopic'
			, {content: '!buytopic test', from: 'a', channel: 'chan'});

		var dAmn = {};
		dAmn.setTopic = function (channel, topic) {
			test.strictEqual(channel, 'chan');
			test.strictEqual(topic, 'test');
		};

		var keyValuePut = self.Context.KeyValueStore.put;
		self.Context.KeyValueStore.put = function (key, value, cb) {
			test.strictEqual(key, 'LastTopicChange');
			test.strictEqual(value.by, 'a');

			keyValuePut.call(
				self.Context.KeyValueStore
				, key
				, value
				, function (success) {
					test.strictEqual(success, true);

					self.Context.maindb.get(
						"SELECT value FROM misc WHERE key = 'LastTopicChange'"
						, function (err, row) {
							var lastTopicChange = JSON.parse(row.value);
							test.strictEqual(err, null);
							test.strictEqual(lastTopicChange.by, 'a');
						}
					);

					self.Context.maindb.get(
						"SELECT balance FROM word_economy_players "
						+ "WHERE user = 'a'"
						, function (err, row) {
							test.strictEqual(err, null);
							test.strictEqual(row.balance, 99);
						}
					);
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) VALUES ('a', 599)"
			, function (err) {
				test.strictEqual(err, null);
				new economy(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testCaseInsensitive: function (test) {
		var self = this;
		test.expect(2);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_give'
			, {content: '!give abc 1', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			self.Context.maindb.all(
				"SELECT * FROM word_economy_players"
				, function (err, rows) {
					test.deepEqual(
						rows
						, [
							{id: 1, user: 'A', balance: 0}
							, {id: 2, user: 'ABC', balance: 200}
						]
					);
				}
			);
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) "
			+ "VALUES ('A', 100), ('ABC', 100)"
			, function (err) {
				test.strictEqual(err, null);
				new economy(self.Context, EventProcessor, dAmn);
			}
		);

		test.done();
	},

	testWordPriceOwned: function (test) {
		var self = this;
		test.expect(3);

		var EventProcessor = testUtils.getMockEventProcessor(
			'cmd_wordprice'
			, {content: '!wordprice word', from: 'a'});

		var dAmn = {};
		dAmn.sendMsg = function (channel, msg) {
			test.strictEqual(
				msg
				, 'a: "word" would cost &pound;20.49 to buy '
				+ 'but is already owned by b.');
		};

		self.Context.maindb.run(
			"INSERT INTO word_economy_players (user, balance) "
			+ "VALUES ('b', 0)"
			, function (err) {
				test.strictEqual(err, null);
				self.Context.maindb.run(
					"INSERT INTO word_economy_words (user_id, word) "
					+ "VALUES (1, 'word')"
					, function (err) {
						test.strictEqual(err, null);
						new economy(self.Context, EventProcessor, dAmn);
					}
				);
			}
		);

		test.done();
	}
};
