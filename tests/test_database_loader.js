var utils = require('../lib/test_utils.js');
var DatabaseLoader = require('../lib/database_loader.js');

var MockDatabase = function (runErr) {
	return function () {
		this.parallelize = function (cb) {
			cb();
		};

		this.get = function (query, cb) {
			cb(null, null);
		};

		this.on = function (evt, fn) {
			if (evt === 'open') {
				fn();
			}
		};

		this.run = function (query, cb) {
			cb(runErr);
		};
	};
};

var MockFS = function (contents) {
	return {
		readFile: function (file, options, fn) {
			fn(null, contents);
		},
		stat: function (path, cb) {
			cb(null, {size: 0});
		}
	};
};

exports.testUnableToOpenDB = function (test) {
	test.expect(2);

	var Context = {sqlite3: {}};
	Context.sqlite3.Database = function (db) {
		this.on = function (evt, fn) {
			if (evt === 'error') {
				fn({message: 'bad'});
			}
		};
	};

	utils.interceptLogging(Context, this);
	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.strictEqual(db, null);
	};

	dbl.load('a', '', fn);
	test.strictEqual(this.error, "Error opening 'a': bad");

	test.done();
};

exports.testTableExistanceCheckFail = function (test) {
	test.expect(2);

	var Context = {sqlite3: {}, fs: {}};

	Context.fs.stat = function (path, cb) {
		cb({message: 'bad'});
	};

	Context.sqlite3.Database = function (db) {
		this.on = function (evt, fn) {
			if (evt === 'open') {
				fn();
			}
		};
	};

	utils.interceptLogging(Context, this);
	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.strictEqual(db, null);
	};

	dbl.load('a', '', fn);
	test.strictEqual(this.error, 'Error checking tables existed: bad');

	test.done();
};

exports.testDBContainsTables = function (test) {
	test.expect(1);

	var Context = {sqlite3: {}, fs: {}};

	Context.fs.stat = function (path, cb) {
		cb(null, {size: 1});
	};

	Context.sqlite3.Database = function (db) {
		this.on = function (evt, fn) {
			if (evt === 'open') {
				fn();
			}
		};
	};

	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.notStrictEqual(db, null);
	};

	dbl.load('a', '', fn);
	test.done();
};

exports.testUnableToReadSQLFile = function (test) {
	test.expect(2);

	var Context = {sqlite3: {}, fs: {}};
	Context.sqlite3.Database = MockDatabase();

	Context.fs.stat = function (path, cb) {
		cb(null, {size: 0});
	};

	Context.fs.readFile = function (file, options, fn) {
		fn({message: 'bad'});
	};

	utils.interceptLogging(Context, this);
	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.strictEqual(db, null);
	};

	dbl.load('a', 'sql', fn);
	test.strictEqual(
		this.error
		, "Unable to initialise table, error "
		+ "while reading SQL file 'sql': bad");

	test.done();
};

exports.testQueryFailure = function (test) {
	test.expect(2);

	var Context = {sqlite3: {}};
	Context.sqlite3.Database = MockDatabase({message: 'bad'});
	Context.fs = MockFS('1;\n\n2;');

	utils.interceptLogging(Context, this);
	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.strictEqual(db, null);
	};

	dbl.load('a', 'sql', fn);
	test.strictEqual(
		this.error
		, 'Unable to initialise table, error running SQL: bad');

	test.done();
};

exports.testRunQueries = function (test) {
	test.expect(1);

	var Context = {sqlite3: {}};
	Context.sqlite3.Database = MockDatabase();
	Context.fs = MockFS('1;\n\n2;');

	var dbl = new DatabaseLoader(Context);

	var fn = function (db) {
		test.notStrictEqual(db, null);
	};

	dbl.load('a', 'sql', fn);
	test.done();
};
