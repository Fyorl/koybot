'use strict';

var help = require('../listeners/cmd_help.js');
var utils = require('../lib/test_utils.js');

exports.testCmdHelp = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor = require('../lib/event_processor.js');
	EventProcessor = new EventProcessor(Context);

	EventProcessor.register('recvMsg', function () {});
	EventProcessor.register('recv', function () {});
	EventProcessor.register('recv', function () {});
	EventProcessor.register('cmd_wordprice', function () {});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, 'Use !help <b>command</b> to get help on a specific command. '
			+ 'Currently available commands are: '
			+ '<b>wordprice</b>, <b>help</b>');
	};

	new help(Context, EventProcessor, dAmn);
	EventProcessor.fire('cmd_help', {content: '!help'});

	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(
			msg
			, "Usage:<br/>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;`help - Print a list of "
			+ "available commands.<br/>"
			+ "&nbsp;&nbsp;&nbsp;&nbsp;`help <b>command</b> - Print "
			+ "detailed usage instructions for the specific command.");
	};

	Context.config = {cmdChar: ['`', '!']};
	EventProcessor.fire('cmd_help', {content: '!help help'});

	dAmn.sendMsg = function (channel, msg) {
		test.ok(false);
	};

	EventProcessor.fire('cmd_help', {content: '!help 404'});

	test.done();
};
