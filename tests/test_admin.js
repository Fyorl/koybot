'use strict';

var admin = require('../admin.js');

exports.testAdd = function (test) {
	test.expect(3);

	var db = {};
	db.run = function (query, values, fn) {
		test.strictEqual(query.indexOf('INSERT INTO'), 0);
		test.deepEqual(values, {$user: 'user'});
		fn(null);
	};

	var done = function (err) {
		test.strictEqual(err, null);
	};

	admin.add(db, 'user', done);

	test.done();
};

exports.testRemove = function (test) {
	test.expect(3);

	var db = {};
	db.run = function (query, values, fn) {
		test.strictEqual(query.indexOf('DELETE FROM'), 0);
		test.deepEqual(values, {$user: 'user'});
		fn(null);
	};

	var done = function (err) {
		test.strictEqual(err, null);
	};

	admin.remove(db, 'user', done);

	test.done();
};

exports.testIsAdmin = function (test) {
	test.expect(4);

	var db = {};
	db.get = function (query, values, fn) {
		test.strictEqual(query.indexOf('SELECT *'), 0);
		test.deepEqual(values, {$user: 'user'});
		fn(null, true);
	};

	var done = function (err, success) {
		test.strictEqual(err, null);
		test.ok(success);
	};

	admin.isAdmin(db, 'user', done);

	test.done();
};
