'use strict';

var prop = require('../lib/property.js');
var utils = require('../lib/test_utils.js');
var Packet = require('../lib/packet.js');

var getMockContext = function (attach) {
	var Context = utils.getMockContext(attach);
	Context.Packet = new Packet(Context);
	Context.cmdLine = {prettyLogs: true};

	return Context;
};

var getProp = function (attach) {
	return new prop(getMockContext(attach));
};

exports.testCorruptPacket = function (test) {
	test.expect(4);

	var p = getProp(this);
	p.handle({cmd: 'property', param: undefined, args: null, body: null});

	test.strictEqual(
		this.error
		, 'Corrupt property packet received: '
		+ '{"cmd":"property","args":null,"body":null}');

	p.handle({cmd: 'property', param: 'bad', args: null, body: null});

	test.strictEqual(
		this.error
		, 'Corrupt property packet received: '
		+ '{"cmd":"property","param":"bad","args":null,"body":null}');

	p.handle({cmd: 'property', param: 'chat:chan', args: null, body: null});

	test.strictEqual(
		this.error
		, 'Corrupt property packet received: '
		+ '{"cmd":"property","param":"chat:chan","args":null,"body":null}');

	p.handle({cmd: 'property', param: 'chat:chan', args: {}, body: null});

	test.strictEqual(
		this.error
		, 'Corrupt property packet received: '
		+ '{"cmd":"property","param":"chat:chan","args":{},"body":null}');

	test.done();
};

exports.testUnhandledProperty = function (test) {
	test.expect(1);

	var p = getProp(this);
	p.handle({
		cmd: 'property'
		, param: 'chat:chan'
		, args: {p: 'bad'}
		, body: null
	});

	test.strictEqual(this.log, 'Unhandled property: bad');

	test.done();
};

exports.testHandleTopic = function (test) {
	test.expect(3);

	var Context = getMockContext(this);
	var EventProcessor = {};
	EventProcessor.fire = function (evt, arg) {
		test.strictEqual(evt, 'property_topic');
		test.deepEqual(arg, {by: 'a', channel: 'chan', value: 'value'});
	};

	var p = new prop(Context, EventProcessor);

	p.handle({
		cmd: 'property'
		, param: 'chat:chan'
		, args: {p: 'topic', by: 'a'}
		, body: 'value'
	});

	test.notStrictEqual(
		this.log.indexOf('\u001B[1m** a changed the topic to:\u001B[0m value')
		, -1);

	test.done();
};
