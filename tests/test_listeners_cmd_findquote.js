'use strict';

var findquote = require('../listeners/cmd_findquote.js');
var utils = require('../lib/test_utils.js');

exports.testNoContent = function (test) {
	test.expect(0);

	var Context = utils.getMockContext(this);
	Context.Packet = utils.getMockPacket();

	Context.maindb.get = function () {
		test.ok(false);
	};

	var data = [
		{content: '!findquote'}
		, {content: '!findquote '}
	];

	data.forEach(function (datum) {
		var EventProcessor = utils.getMockEventProcessor(datum);
		new findquote(Context, EventProcessor);
	});

	test.done();
};

exports.testFindByID = function (test) {
	test.expect(3);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!findquote #666'});

	Context.maindb.get = function (query, values) {
		test.strictEqual(values.$id, '666');
		test.equal(values.$quote, null);
		test.equal(values.$quoted, null);
	};

	var q = new findquote(Context, EventProcessor);

	test.done();
};

exports.testFindByUserFails = function (test) {
	test.expect(0);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!findquote &lt;user&gt; '});

	Context.maindb.get = function (query, values) {
		test.ok(false);
	};

	var q = new findquote(Context, EventProcessor);

	EventProcessor =
		utils.getMockEventProcessor({content: '!findquote &lt;user&gt;'});

	q = new findquote(Context, EventProcessor);

	test.done();
};

exports.testFindByUser = function (test) {
	test.expect(3);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({
			content: '!findquote &lt;user&gt; needle'
		});

	Context.maindb.get = function (query, values) {
		test.strictEqual(values.$quoted, 'user');
		test.strictEqual(values.$quote, '%needle%');
		test.equal(values.$id, null);
	};

	var q = new findquote(Context, EventProcessor);

	test.done();
};

exports.testFind = function (test) {
	test.expect(3);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({content: '!findquote needle'});

	Context.maindb.get = function (query, values) {
		test.strictEqual(values.$quote, '%needle%');
		test.equal(values.$id, null);
		test.equal(values.$quoted, null);
	};

	var q = new findquote(Context, EventProcessor);

	test.done();
};

exports.testDBError = function (test) {
	test.expect(1);

	var Context = utils.getMockContext(this);
	Context.maindb.get = utils.injectError();

	var EventProcessor =
		utils.getMockEventProcessor({content: '!findquote needle'});

	var q = new findquote(Context, EventProcessor);
	test.strictEqual(this.error, 'Unable to search for quotes: bad');

	test.done();
};

exports.testFormatting = function (test) {
	test.expect(2);

	var Context = utils.getMockContext(this);
	var EventProcessor =
		utils.getMockEventProcessor({
			content: '!findquote needle'
			, channel: 'chan'
		});

	var dAmn = {};
	dAmn.sendMsg = function (channel, msg) {
		test.strictEqual(channel, 'chan');
		test.strictEqual(
			msg
			, 'Quote #666, quoted by SirQuotesALot on 01 Jan 1970 00:00:00: '
			+ '&lt;SirGetsQuotedALot&gt; needle :)');
	};

	Context.maindb.get = function (query, values, cb) {
		cb(null, {
			id: 666
			, time_quoted: 1
			, quoter: 'SirQuotesALot'
			, quoted: 'SirGetsQuotedALot'
			, quote: 'needle &emote\t:)\t15\t15\t:)\t???\t'
		});
	};

	var q = new findquote(Context, EventProcessor, dAmn);

	test.done();
};

