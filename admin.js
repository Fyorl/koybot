'use strict';

(function () {
	var path = require('path');
	var Context = {};

	Context.sqlite3 = require('sqlite3');
	Context.fs = require('fs');

	var Log = require('./lib/log.js');
	var DatabaseLoader = require('./lib/database_loader.js');

	Context.Log = new Log();
	var loader = new DatabaseLoader(Context);

	var incorrectInvocation = function () {
		Context.Log.error('Usage: node admin.js add|remove username');
		process.exit(1);
	};

	if (process.argv.length < 2
		|| path.basename(process.argv[1]) === 'admin.js') {

		// We are running on the command line.
		if (process.argv.length < 4) {
			incorrectInvocation();
		}

		loader.load('db/main.sq3', 'db/main.sql', function (db) {
			if (db == null) {
				Context.Log.error('Unable to read db/main.sq3');
				process.exit(1);
			}

			var command = process.argv[2];
			var username = process.argv[3];
			var verb;

			var adminDone = function (err, who) {
				if (err == null) {
					Context.Log.log('Successfully %s "%s".', verb, who);
					process.exit(0);
				} else {
					Context.Log.error(
						'There was an error %s "%s" to the database: %s'
						, verb
						, who
						, err.message);

					process.exit(1);
				}
			};

			switch (command) {
				case 'add':
					verb = 'added';
					exports.add(db, username, adminDone);
					break;

				case 'remove':
					verb = 'removed';
					exports.remove(db, username, adminDone);
					break;

				default:
					incorrectInvocation();
			}
		});
	}
})();

exports.add = function (db, admin, fn) {
	var query =
		'INSERT INTO admin '
		+ '(user) '
		+ 'VALUES ($user)';

	var values = {$user: admin};

	db.run(query, values, function (err) {
		fn(err, admin);
	});
};

exports.remove = function (db, admin, fn) {
	var query =
		'DELETE FROM admin '
		+ 'WHERE user = $user '
		+ 'COLLATE NOCASE';

	var values = {$user: admin};

	db.run(query, values, function (err) {
		fn(err, admin);
	});
};

exports.isAdmin = function (db, admin, fn) {
	var query =
		'SELECT * '
		+ 'FROM admin '
		+ 'WHERE user = $user '
		+ 'COLLATE NOCASE';

	var values = {$user: admin};

	db.get(query, values, function (err, row) {
		fn(err, row != null);
	});
};
