'use strict';

// TODO: Refactor this because it is unreadable, unmaintainable garbage.
// Global y variable.
// Input line should be represented as one long string.

module.exports = function (onInput) {
	var self = this;
	var BACKSPACE = 127; // ncurses library seems to have this wrong on linux.
	var ENTER = 10;      // and this...
	var MAX_INPUT_LINES = 5;
	var MAX_OUTPUT_LINES = 1024;

	var curses = require('ncurses');

	var prompt = '> ';
	var win = new curses.Window();
	win.addstr(win.height - 1, 0, prompt);
	win.refresh();

	var pre = prompt.length;
	var outputLines = [];
	var inputLines = [''];
	var inputIndex = 0;
	var x = win.curx;

	self.close = function () {
		win.close();
	};

	self.write = function (line) {
		if (line.length < 1) {
			return;
		}

		var i = line.length - 1;
		while (i > -1) {
			if (line[i] !== '\n' && line[i] !== '\r' && line[i] !== '\0') {
				break;
			}

			i--;
		}

		if (i === 0) {
			return;
		}

		line = line.substring(0, i + 1).replace(/	/g, ' ');
		var lines = line.split('\n');

		while (lines.length > 0) {
			if (outputLines.length >= MAX_OUTPUT_LINES) {
				outputLines.shift();
			}

			var l = lines.shift();
			outputLines.push(l);
		}

		var saveX = win.curx;
		var saveY = win.cury;

		updateLines();

		win.cursor(saveY, saveX);
		win.refresh();
	};

	var lineBuffer = function (update) {
		if (update == null) {
			return inputLines[inputIndex];
		}

		inputLines[inputIndex] = update;
		return inputLines[inputIndex];
	};

	var clearln = function (row) {
		win.cursor(row, 0);
		win.clrtoeol();
	};

	var sequenceToAttribute = function (seq) {
		switch (seq) {
			case '\u001b[1m': return curses.attrs.BOLD;
			case '\u001b[0m': return curses.attrs.NORMAL;
			default:          return null;
		}
	};

	var parseEscapeSequences = function (line) {
		var info = {seq: {}};
		var re = /\u001b\[\d+m/;

		var replacer = function (match, offset) {
			info.seq[offset] = sequenceToAttribute(match);
			return '';
		};

		while (re.test(line)) {
			line = line.replace(re, replacer);
		}

		info.text = line;

		return info;
	};

	var updateLines = function () {
		var i, n, row;

		for (i = 0; i < inputLines.length; i++) {
			n = inputLines.length - i - 1;
			row = win.height - i - 1;
			clearln(row);
			win.addstr(row, 0, ((n === 0) ? prompt: '') + inputLines[n]);
		}

		var startRow = win.height - inputLines.length - 1;
		for (i = outputLines.length - 1; i >= 0; i--) {
			if (startRow < 0) {
				break;
			}

			var lineInfo = parseEscapeSequences(outputLines[i]);
			var seqs = Object.getOwnPropertyNames(lineInfo.seq);
			var seqidx = 0;
			var lastseq = curses.attrs.NORMAL;
			var lastidx = 0;
			var line = lineInfo.text;
			var rowsTaken = Math.ceil(line.length / win.width);
			for (n = 0; n < rowsTaken; n++) {
				row = startRow - rowsTaken + 1 + n;
				if (row < 0) {
					continue;
				}

				clearln(row);
				win.addstr(row, 0, line.substr(n * win.width, win.width));

				var lower = n * win.width;
				var upper = lower + win.width;
				for (; seqidx < seqs.length; seqidx++) {
					var diff = win.width;
					var seqchar = parseInt(seqs[seqidx]);

					if (seqchar >= lower && seqchar < upper) {
						diff = seqchar - lastidx;
					}

					win.chgat(row, lastidx, diff, lastseq);

					if (seqchar >= lower && seqchar < upper) {
						lastseq = lineInfo.seq[seqchar];
						lastidx = parseInt(seqchar);
					} else {
						break;
					}
				}
			}

			startRow -= rowsTaken;
		}

		for (i = startRow; i >= 0; i--) {
			clearln(i);
		}
	};

	var insertChar = function (pos, char) {
		var str = lineBuffer();
		var a = Array.prototype.slice.call(str);
		a.splice(pos, 0, char);
		str = a.join('');
		lineBuffer(str);

		for (var i = inputIndex; i < inputLines.length; i++) {
			var line = inputLines[i];
			var nextLine = inputLines[i + 1];

			if (line.length + ((i === 0) ? pre : 0) <= win.width) {
				break;
			}

			var s = Array.prototype.slice.call(line);
			var c = s.pop();

			if (nextLine == null) {
				nextLine = c;
			} else {
				nextLine = c + nextLine;
			}

			inputLines[i] = s.join('');
			inputLines[i + 1] = nextLine;
		}

		x++;
		if (x >= win.width) {
			newLine();
		}
	};

	var deleteChar = function (pos) {
		var str = lineBuffer();
		var a = Array.prototype.slice.call(str);
		a.splice(pos, 1);
		str = a.join('');
		lineBuffer(str);

		for (var i = inputIndex; i < inputLines.length; i++) {
			var line = inputLines[i];
			var nextLine = inputLines[i + 1];

			if (nextLine == null) {
				break;
			}

			if (nextLine === '') {
				delLine();
				break;
			}

			var s = Array.prototype.slice.call(nextLine);
			var c = s.shift();
			line += c;

			inputLines[i + 1] = s.join('');
			inputLines[i] = line;

			if (inputLines[i + 1] === '') {
				delLine();
				break;
			}
		}
	};

	var newLine = function () {
		inputIndex++;

		if (inputIndex >= MAX_INPUT_LINES) {
			x = win.width - 1;
			return;
		}

		if (inputLines[inputIndex] == null) {
			inputLines[inputIndex] = '';
		}

		pre = 0;
	};

	var delLine = function () {
		if (inputLines.length < 2) {
			return;
		}

		clearln(win.height - inputLines.length);
		inputLines.pop();
	};

	var upLine = function () {
		var newIndex = inputIndex - 1;
		var newY = win.cury - 1;
		if (newIndex < 0 || newY < 0) {
			return;
		}

		inputIndex = newIndex;

		if (inputIndex === 0) {
			pre = prompt.length;
		}

		return newY;
	};

	var downLine = function () {
		var newIndex = inputIndex + 1;
		var newY = win.cury + 1;

		if (newIndex >= inputLines.length || newIndex >= MAX_INPUT_LINES) {
			return;
		}

		inputIndex = newIndex;
		pre = 0;

		return newY;
	};

	var getY = function () {
		return win.height - inputLines.length + inputIndex;
	};

	var backspace = function () {
		var newX = win.curx - 1;
		if (newX < pre) {
			if (upLine() == null) {
				return;
			}

			x = lineBuffer().length + pre - 1;
		} else {
			x = newX;
		}

		deleteChar(x - pre);
		updateLines();
		win.cursor(getY(), x);
	};

	var del = function () {
		if (x >= lineBuffer().length + pre) {
			return;
		}

		deleteChar(x - pre);
		updateLines();
		win.cursor(getY(), x);
	};

	var left = function () {
		var newX = win.curx - 1;
		var newY = win.cury;

		if (newX < pre) {
			if (inputIndex > 0) {
				newY = upLine();
				newX = win.width - 1;
			} else {
				return;
			}
		}

		x = newX;
		win.cursor(newY, newX);
	};

	var right = function () {
		var newX = win.curx + 1;
		var newY = win.cury;

		if (newX >= win.width) {
			if (inputIndex < inputLines.length - 1) {
				newY = downLine();
				newX = 0;

				if (newY == null) {
					return;
				}
			} else {
				return;
			}
		} else if (newX > lineBuffer().length + pre) {
			return;
		}

		x = newX;
		win.cursor(newY, newX);
	};

	var up = function () {
		var newY = upLine();

		if (newY == null) {
			return;
		}

		if (inputIndex === 0 && x < pre) {
			x = pre;
		}

		if (x > lineBuffer().length + pre) {
			x = lineBuffer().length + pre;
		}

		if (x >= win.width) {
			x = win.width - 1;
		}

		win.cursor(newY, x);
	};

	var down = function () {
		var newY = downLine();

		if (newY == null) {
			return;
		}

		if (x > lineBuffer().length + pre) {
			x = lineBuffer().length + pre;
		}

		if (x >= win.width) {
			x = win.width - 1;
		}

		win.cursor(newY, x);
	};

	var add = function (char) {
		if (inputLines.length >= MAX_INPUT_LINES
			&& inputLines[MAX_INPUT_LINES - 1].length >= win.width - 1) {

			return;
			}

			var newY = win.cury;
		if (inputLines[inputLines.length - 1].length
			+ ((inputLines.length === 1) ? pre : 0) + 1 > win.width) {

			newY--;
			}

			insertChar(x - pre, char);

		if (x >= win.width) {
			newY++;
			if (newY >= win.height) {
				newY--;
			}
			x = 0;
		}

		updateLines();
		win.cursor(newY, x);
	};

	var home = function () {
		x = pre;
		win.cursor(win.cury, x);
	};

	var end = function () {
		var newX = lineBuffer().length + pre;
		if (newX >= win.width) {
			newX = win.width - 1;
		}

		x = newX;
		win.cursor(win.cury, x);
	};

	var enter = function () {
		if (typeof onInput !== 'function') {
			return;
		}

		var input = inputLines.join('');
		inputLines = [''];
		inputIndex = 0;
		pre = prompt.length;
		x = pre;

		if (input === '/refresh') {
			win.clear();
		}

		updateLines();
		win.cursor(win.height - 1, pre);

		if (input !== '/refresh') {
			onInput(input);
		}
	};

	win.on('inputChar', function (char, charCode, isKey) {
		switch (charCode) {
			case BACKSPACE:         backspace(); break;
			case ENTER:             enter();     break;
			case curses.keys.LEFT:  left();      break;
			case curses.keys.RIGHT: right();     break;
			case curses.keys.UP:    up();        break;
			case curses.keys.DOWN:  down();      break;
			case curses.keys.DEL:   del();       break;
			case curses.keys.HOME:  home();      break;
			case curses.keys.END:   end();       break;
			default:                add(char);
		}

		win.refresh();
	});
};
