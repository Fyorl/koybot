'use strict';

module.exports = function (Context) {
	this.create = function (size, initFn) {
		return new ResourcePool(size, initFn);
	};

	var ResourcePool = function (size, initFn) {
		var self = this;
		var pool = [];
		var lock = Context.Locks.create();

		for (var i = 0; i < size; i++) {
			pool.push(initFn(i));
		}

		self.release = function (resource) {
			pool.push(resource);
			lock.release();
		};

		self.acquire = function (onComplete) {
			var resource = pool.pop();

			if (resource === undefined) {
				// The pool is empty, we have to acquire the lock to ensure we
				// get the resource as soon as one becomes available.
				lock.acquire(self.acquire.curry(onComplete));
			} else {
				onComplete(resource);
			}
		};
	};
};
