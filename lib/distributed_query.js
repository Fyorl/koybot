'use strict';

// Library for querying over a set of sqlite3 databases partitioned by day.
// TODO: This is ugly and needs refactoring. Needs dedicated classes for
// managing the buffer and the whole mess around locking files.
// Might be cleaner to not try to do any caching or sharing of files and just
// copy to a new directory for each query then delete afterwards.
// Will have to benchmark the performance hit.

module.exports = function (Context, source, filenameFn) {
	if (Context == null || source == null || filenameFn == null) {
		throw new Error('Insufficient arguments to instantiate class.');
	}

	var self = this;

	self.QueryType = Object.freeze({
		EXEC: 1
		, ONE: 2
		, ALL: 3
		, EACH: 4
	});

	self.QueryMode = Object.freeze({
		ORDERED: 1
		, UNORDERED: 2
	});

	// Setup some sensible defaults.
	var today = Context.moment().utc();
	var endDay = today.startOf('day');
	var startDay = Context.moment(today).subtract(14, 'days').startOf('day');
	var cacheDir = 'cache/';
	var maxUnzipAttempts = 10;
	var maxCacheSize = 128;
	var lockPoolSize = 128;

	if (Context.config != null
		&& Context.config.distQueryMaxCacheSize != null) {

		maxCacheSize = Context.config.distQueryMaxCacheSize;
	}

	if (Context.config != null
		&& Context.config.distQueryCacheDirectory != null) {

		cacheDir = Context.config.distQueryCacheDirectory;
	}

	// Do some initialisation.
	try {
		Context.fs.mkdirSync(cacheDir);
	} catch (ignored) {}

	var cache = Context.Cache.create(maxCacheSize);
	var toDelete = {};
	cache.on('pop', function (sq3File, dbHandle) {
		toDelete[sq3File] = true;
	});

	// We will manage a set of prepare locks to ensure all the I/O operations
	// do not interfere with each other in cases where one query is executing
	// and then another query tries to execute over an overlapping date range.

	// In such circumstances, if one file is being prepared and then we attempt
	// to prepare the file again, the behaviour is undefined.

	// In order to not exhaust lock IDs, we manage a lock pool which effectively
	// limits the amount of file preparations that can be going on at once.

	var activeFiles = {};
	var activeLocks = {};
	var lockPool = Context.Pool.create(lockPoolSize, function (index) {
		return Context.Locks.create();
	});

	// We try to imitate the node-sqlite3 API's argument flexibility here.
	var sqlite3APIToOptions = function (a, b, c) {
		var options = {};
		options.query = a;

		if (c == null && typeof b === 'function') {
			// The 'b' must be our output function and values is being omitted.
			options.outputFn = b;
		} else {
			options.values = b;
			options.outputFn = c;
		}

		return options;
	};

	var parseDate = function (date) {
		var parseAttempt = Context.moment(date).utc().startOf('day');
		if (isNaN(parseAttempt.valueOf())) {
			return false;
		}

		return parseAttempt;
	};

	var extractFilename = function (path) {
		var splitOnSlash = path.split('/');
		return splitOnSlash[splitOnSlash.length - 1];
	};

	var prepareSQLiteFile = function (lock, day, src, dest, onComplete) {
		if (activeFiles[day.valueOf()] === undefined) {
			activeFiles[day.valueOf()] = Context.RefCount.create();
		}

		activeFiles[day.valueOf()].allocate();

		var openAndCacheDB = function (sq3File) {
			var db = new Context.sqlite3.Database(sq3File);

			db.on('error', function (err) {
				onComplete(err, lock, day);
			});

			db.on('open', function () {
				cache.put(sq3File, db);
				onComplete(null, lock, day, db);
			});
		};

		var unzipAttempts = 0;
		var tryUnzip = function (archive) {
			Context.FileUtils.unzip(archive, function (code, sq3File) {
				if (code === 0) {
					openAndCacheDB(sq3File);
				} else {
					// There's a small chance that a file may not have been
					// deleted in time so we retry a few times.
					unzipAttempts++;
					if (unzipAttempts >= maxUnzipAttempts) {
						onComplete(
							new Error('Unable to extract ' + src)
							, lock
							, day);
					} else {
						setTimeout(tryUnzip.curry(archive), 10);
					}
				}
			});
		};

		Context.FileUtils.copyFile(src, dest, function (err) {
			if (err == null) {
				tryUnzip(dest);
			} else {
				onComplete(err, lock, day);
			}
		});
	};

	self.setQueryRange = function (newStartDay, newEndDay) {
		var parseAttemptStartDay = parseDate(newStartDay);
		var parseAttemptEndDay = parseDate(newEndDay);
		var errors = [];

		if (!parseAttemptStartDay) {
			if (newStartDay != null) {
				errors.push(Context.fmt(
					'Cannot set start date to "%s".'
					, newStartDay));
			}
		} else {
			startDay = parseAttemptStartDay;
		}

		if (!parseAttemptEndDay) {
			if (newEndDay != null) {
				errors.push(Context.fmt(
					'Cannot set end date to "%s".'
					, newEndDay));
			}
		} else {
			endDay = parseAttemptEndDay;
		}

		return errors;
	};

	self.query = function (options) {
		var start = startDay;
		var end = endDay;
		var mode = self.QueryMode.UNORDERED;
		var errors = [];

		if (options.start != null) {
			var newStart = parseDate(options.start);
			if (!newStart) {
				errors.push(Context.fmt(
					'Cannot set query specific start date "%s".'
					, options.start));
			} else {
				start = newStart;
			}
		}

		if (options.end != null) {
			var newEnd = parseDate(options.end);
			if (!newEnd) {
				errors.push(Context.fmt(
					'Cannot set query specific end date "%s".'
					, options.end));
			} else {
				end = newEnd;
			}
		}

		if (options.type == null) {
			errors.push('No query type specified.');
		} else {
			var found = false;
			for (var prop in self.QueryType) {
				if (self.QueryType[prop] === options.type) {
					found = true;
					break;
				}
			}

			if (!found) {
				errors.push(
					Context.fmt('Unknown query type: %s', options.type));
			}
		}

		if (errors.length > 0) {
			return errors;
		}

		if (options.mode) {
			mode = options.mode;
		}

		if (start > end) {
			var tmp = start;
			start = end;
			end = tmp;
		}

		var getSQ3Filename = function (day) {
			var src = filenameFn(day);
			var dest = cacheDir + extractFilename(src);

			return dest.substring(0, dest.length - 4);
		};

		var checkCache = function (day) {
			var unzippedName = getSQ3Filename(day);
			var cachedDB = cache.get(unzippedName);
			if (cachedDB !== undefined) {
				prepared(null, null, day, cachedDB);
				return true;
			}

			return false;
		};

		var acquireLock = function (day, lock) {
			if (checkCache(day)) {
				// Don't need the lock anymore.
				lockPool.release(lock);
				return;
			}

			activeLocks[day.valueOf()] = {
				lock: lock
				, refcount: Context.RefCount.create()
			};

			activeLocks[day.valueOf()].refcount.allocate();
			lock.acquire(prepare.curry(lock, day));
		};

		var freeActiveLock = function (day) {
			var lockResource = activeLocks[day.valueOf()];
			lockResource.refcount.free();

			if (!lockResource.refcount.inUse()) {
				lockPool.release(lockResource.lock);
				delete activeLocks[day.valueOf()];
			}
		};

		var prepare = function (lock, day) {
			if (checkCache(day)) {
				// Don't need to prepare anymore.
				lock.release();
				freeActiveLock(day);
				return;
			}

			var src = source + filenameFn(day);
			var dest = cacheDir + extractFilename(src);
			prepareSQLiteFile(lock, day, src, dest, prepared);
		};

		var prepared = function (err, lock, day, db) {
			if (lock == null) {
				// We came here from the cache so activeFiles wasn't set in the
				// prepare step.
				if (activeFiles[day.valueOf()] === undefined) {
					activeFiles[day.valueOf()] = Context.RefCount.create();
				}

				activeFiles[day.valueOf()].allocate();
			} else {
				lock.release();
				freeActiveLock(day);
			}

			if (err != null) {
				Context.Log.error(
					'There was an error running the query for %s: %s'
					, day.format('DD/MM/YYYY')
					, err.message);

				failures[day.valueOf()] = true;
				done(day);
				return;
			}

			switch (options.type) {
				case self.QueryType.EXEC:
					runQuery(db.run, db, options, day);
					break;

				case self.QueryType.ONE:
					runQuery(db.get, db, options, day);
					break;

				case self.QueryType.ALL:
					runQuery(db.all, db, options, day);
					break;

				case self.QueryType.EACH:
					runIteratorQuery(db.each, db, options, day);
					break;
			}
		};

		var runQuery = function (dbFn, db, options, day) {
			var args = [];
			args.push(options.query);

			if (options.values != null) {
				args.push(options.values);
			}

			args.push(function (err, out) {
				done(day);
				if (mode === self.QueryMode.UNORDERED) {
					options.outputFn(day.valueOf(), err, out);
				} else {
					performBuffering(day, err, out);
				}
			});

			dbFn.apply(db, args);
		};

		var runIteratorQuery = function (dbFn, db, options, day) {
			var args = [];
			args.push(options.query);

			if (options.values != null) {
				args.push(options.values);
			}

			args.push(function (err, out) {
				if (mode === self.QueryMode.UNORDERED) {
					options.outputFn(day.valueOf(), err, out);
				} else {
					performIteratorBuffering(day, err, out);
				}
			});

			args.push(function (err, numRows) {
				done(day);
				if (options.onComplete != null
					&& mode === self.QueryType.UNORDERED) {

					options.onComplete(day.valueOf(), err, numRows);
				} else {
					finaliseIteratorBuffering(day, err, numRows);
				}
			});

			dbFn.apply(db, args);
		};

		var performIteratorBuffering = function (day, err, out) {
			var bufferDay = Context.moment(start).add(bufferPosition, 'days');
			while (failures[bufferDay.valueOf()] !== undefined
				&& bufferDay <= end) {

				bufferPosition++;
				bufferDay.add(1, 'day');
			}

			if (bufferDay.valueOf() === day.valueOf()) {
				options.outputFn(day.valueOf(), err, out);
			} else {
				var bufferItem = buffer[day.valueOf()];
				if (bufferItem === undefined) {
					bufferItem = ['null', []];
				}

				bufferItem[0] = JSON.stringify(err);
				bufferItem[1].push(JSON.stringify(out));
				buffer[day.valueOf()] = bufferItem;
			}
		};

		// This method checks if we received the finalise event for the current
		// buffer and stores the event if not. If it is the finalise event for
		// the current buffer bucket then we emit the finalise event if the
		// caller asked for it and delete the current buffer bucket. We then
		// loop over the items in the next buffer bucket and send those to the
		// caller. If we detect that the next bucket has also been finalised,
		// we emit that finalise event and then move onto the next buffer
		// bucket.

		var finaliseIteratorBuffering = function (day, err, numRows) {
			var bufferDay = Context.moment(start).add(bufferPosition, 'days');

			if (bufferDay.valueOf() === day.valueOf()) {
				delete buffer[day.valueOf()];
				bufferPosition++;
				if (options.onComplete != null) {
					options.onComplete(day.valueOf(), err, numRows);
				}

				bufferDay.add(1, 'day');

				while (failures[bufferDay.valueOf()] !== undefined
					&& bufferDay <= end) {

					bufferPosition++;
					bufferDay.add(1, 'day');
				}

				while (buffer[bufferDay.valueOf()] !== undefined
					&& bufferDay <= end) {

					var bufferItem = buffer[bufferDay.valueOf()];
					if (bufferItem[1] != null) {
						for (var i = 0; i < bufferItem[1].length; i++) {
							options.outputFn(
								bufferDay.valueOf()
								, JSON.parse(bufferItem[0])
								, (bufferItem[1][i] === undefined)
									? undefined
									: JSON.parse(bufferItem[1][i]));
						}
					}

					var finaliseBufferItem =
						finaliseEventsBuffer[bufferDay.valueOf()];

					if (finaliseBufferItem === undefined) {
						// If we reach here then the current buffer is empty but
						// there are still more rows to come. These will be
						// handled by performIteratorBuffering() as
						// bufferPosition has been appropriately advanced.
						break;
					} else {
						if (options.onComplete != null) {
							options.onComplete(
								bufferDay.valueOf()
								, JSON.parse(finaliseBufferItem[0])
								, finaliseBufferItem[1]);
						}

						delete buffer[bufferDay.valueOf()];
						delete finaliseBufferItem[bufferDay.valueOf()];
						bufferPosition++;
						bufferDay.add(1, 'day');

						while (failures[bufferDay.valueOf()] !== undefined
							&& bufferDay <= end) {

							bufferPosition++;
							bufferDay.add(1, 'day');
						}
					}
				}
			} else {
				finaliseEventsBuffer[day.valueOf()] =
					[JSON.stringify(err), numRows];
			}
		};

		var performBuffering = function (day, err, out) {
			// Potential target for optimisation?
			buffer[day.valueOf()] = [JSON.stringify(err), JSON.stringify(out)];

			var bufferDay = Context.moment(start).add(bufferPosition, 'days');
			while (failures[bufferDay.valueOf()] !== undefined
				&& bufferDay <= end) {

				bufferPosition++;
				bufferDay.add(1, 'day');
			}

			while (buffer[bufferDay.valueOf()] !== undefined
				&& bufferDay <= end) {

				var bufferItem = buffer[bufferDay.valueOf()];
				options.outputFn(
					bufferDay.valueOf()
					, JSON.parse(bufferItem[0])
					, (bufferItem[1] === undefined)
						? undefined
						: JSON.parse(bufferItem[1]));

				delete buffer[bufferDay.valueOf()];
				bufferPosition++;
				bufferDay.add(1, 'day');

				while (failures[bufferDay.valueOf()] !== undefined
					&& bufferDay <= end) {

					bufferPosition++;
					bufferDay.add(1, 'day');
				}
			}
		};

		var done = function (day) {
			activeFiles[day.valueOf()].free();
			var sq3File = getSQ3Filename(day);

			if (!activeFiles[day.valueOf()].inUse()
				&& cache.get(sq3File) === undefined
				&& toDelete[sq3File]) {

				delete toDelete[sq3File];
				Context.fs.unlink(sq3File, function (err) {
					if (err != null) {
						Context.Log.error(
							'Unable to delete %s, '
							+ 'it may need to be deleted manually: %s'
							, sq3File
							, err.message);
					}
				});
			}
		};

		var buffer = {};
		var failures = {};
		var finaliseEventsBuffer = {};
		var bufferPosition = 0;
		var current = Context.moment(start);
		while (end.diff(current, 'days') > -1) {
			if (checkCache(Context.moment(current))) {
				current.add(1, 'day');
				continue;
			}

			// There are two places we can wait here. First, we wait for a lock
			// to become free from the lock pool. Once we have a lock, we add it
			// to the list of active locks and then lock the preparation. If
			// we want to prepare the same file again, it will wait for the
			// first file to be prepared.

			var lockResource = activeLocks[current.valueOf()];
			if (lockResource === undefined) {
				lockPool.acquire(acquireLock.curry(Context.moment(current)));
			} else {
				lockResource.refcount.allocate();
				lockResource.lock.acquire(
					prepare.curry(lockResource.lock, Context.moment(current)));
			}

			current.add(1, 'day');
		}
	};

	// We provide some ease-of-use methods which allow this library to be
	// queried in a similar way to the node-sqlite3 package.
	self.run = function (a, b, c) {
		var options = sqlite3APIToOptions(a, b, c);
		options.type = self.QueryType.EXEC;
		return self.query(options);
	};

	self.get = function (a, b, c) {
		var options = sqlite3APIToOptions(a, b, c);
		options.type = self.QueryType.ONE;
		return self.query(options);
	};

	self.all = function (a, b, c) {
		var options = sqlite3APIToOptions(a, b, c);
		options.type = self.QueryType.ALL;
		return self.query(options);
	};

	self.each = function (a, b, c, d) {
		var options = {};
		options.type = self.QueryType.EACH;
		options.query = a;

		if (d == null) {
			if (typeof b == 'function' && typeof c == 'function') {
				// Then values is being omitted.
				options.outputFn = b;
				options.onComplete = c;
			} else {
				// Then the onComplete function is being omitted.
				options.values = b;
				options.outputFn = c;
			}
		} else {
			options.values = b;
			options.outputFn = c;
			options.onComplete = d;
		}

		return self.query(options);
	};
};
