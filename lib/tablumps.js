'use strict';

module.exports = function (Context) {
	var self = this;

	var numArgs = Object.freeze({
		'b': 0, 'i': 0, 'u': 0, 'sub': 0, 'sup': 0, 's': 0, 'p': 0, 'br': 0
		, 'code': 0, 'bcode': 0, 'li': 0, 'ul': 0, 'ol': 0, '/b': 0, '/i': 0
		, '/u': 0, '/sub': 0, '/sup': 0, '/s': 0, '/p': 0, '/code': 0
		, '/bcode': 0, '/li': 0, '/ul': 0, '/ol': 0, '/abbr': 0, '/acro': 0
		, '/a': 0, '/iframe': 0, '/embed': 0
		, 'abbr': 1, 'acro': 1
		, 'a': 2, 'dev': 2, 'avatar': 2
		, 'img': 3, 'iframe': 3, 'embed': 3, 'link': 3
		, 'emote': 5
		, 'thumb': 6
	});

	var formatStrings = Object.freeze({
		'b': '<strong>', 'i': '<em>', 's': '<del>', 'br': '<br />'
		, 'bcode': '<pre><code>', '/b': '</strong>', '/i': '</em>'
		, '/s': '</del>', '/bcode': '</code></pre>', '/acro': '</acronym>'
		, 'abbr': '<abbr title="$1">'
		, 'acro': '<acronym title="$1">'
		, 'a': '<a href="$1" title="$2">'
		, 'dev': ':dev$2:'
		, 'avatar': ':icon$1:'
		, 'img': '<img src="$1" height="$2" width="$3" />'
		, 'iframe': '<iframe src="$1" height="$2" width="$3">'
		, 'embed': '<embed src="$1" height="$2" width="$3">'
		, 'emote': '$1'
		, 'thumb': ':thumb$1:'
	});

	var parseLink = function (args) {
		if (args.length < 1) {
			return '<a href="#">[link]</a>';
		}

		if (args.length === 1) {
			return Context.fmt(
				'<a href="%s" title="%s">[link]</a>'
				, args[0]
				, args[0]);
		}

		if (args.length >= 2) {
			return Context.fmt(
				'<a href="%s" title="%s">%s</a>'
				, args[0]
				, args[0]
				, args[1]);
		}
	};

	var parseTablump = function (key, args) {
		// Handle the special case 'link' tablump.
		if (key === 'link') {
			return parseLink(args);
		}

		var result = formatStrings[key];

		if (result === undefined) {
			result = Context.fmt('<%s>', key);
			return result;
		}

		if (numArgs[key] < 1) {
			return result;
		}

		args.forEach(function (arg, n) {
			var regex = new RegExp('\\$' + (n + 1), 'g');
			result = result.replace(regex, arg);
		});

		// If the tablump was prematurely terminated, we replace all the left
		// over markers with a blank string.
		return result.replace(/\$[1-9]/g, '');
	};

	var findAndReplace = function (haystack, needles) {
		needles.forEach(function (needle) {
			haystack = haystack.replace(needle.find, needle.replace);
		});

		return haystack;
	};

	self.convert = function (str) {
		var state = 0;
		var buf = '';
		var wholeBuf = '';
		var tablump = {key: '', expectedArgs: 0, args: []};
		var tablumps = [];

		var endOfTablump = function () {
			tablumps.push({
				find: wholeBuf
				, replace: parseTablump(tablump.key, tablump.args)
			});

			state = 0;
			tablump = {key: '', expectedArgs: 0, args: []};
			buf = '';
			wholeBuf = '';
		};

		var endOfTablumpKey = function () {
			var expectedArgs = numArgs[buf];
			if (expectedArgs === undefined) {
				Context.Log.error('Encountered bad tablump: %s', buf);
				state = 0;
			} else {
				tablump.key = buf;
				tablump.expectedArgs = expectedArgs;
				tablump.args = [];
				state = 2;

				if (expectedArgs === 0) {
					endOfTablump();
				}
			}

			buf = '';
		};

		var endOfArg = function () {
			tablump.args.push(buf);

			if (tablump.args.length === tablump.expectedArgs) {
				endOfTablump();
			}

			buf = '';
		};

		var prematureTermination = function () {
			if (state === 1) {
				tablump.key = buf;
			} else if (state === 2) {
				tablump.args.push(buf);
			}

			endOfTablump();
		};

		// We go one further than the last character in the string so we have
		// a way to know we are at the end of the string (c will be undefined).
		for (var i = 0; i <= str.length; i++) {
			var c = str[i];
			switch (state) {
				case 0:
					if (c === '&') {
						state = 1;
						wholeBuf += c;
					}
					break;

				case 1:
					wholeBuf += (c === undefined) ? '' : c;
					if (c === '\t') endOfTablumpKey();
					else if (c === undefined) prematureTermination();
					else buf += c;
					break;

				case 2:
					wholeBuf += (c === undefined) ? '' : c;
					if (c === '&' && tablump.key === 'link') {
						if (str[i + 1] === '\t') {
							wholeBuf += '\t';
						}

						endOfTablump();
						break;
					}

					if (c === '\t') endOfArg();
					else if (c === undefined) prematureTermination();
					else buf += c;
					break;

				default:
					Context.Log.error(
						'Invalid state encountered parsing tablump.');
			}
		}

		return findAndReplace(str, tablumps);
	};
};

