'use strict';

var moment = require('moment');

exports.getPrettyTime = function (date) {
	if (date == null) {
		return 'DateError';
	}

	date = moment(date).utc();
	return date.format('HH:mm:ss');
};

exports.getPrettyDate = function (date) {
	if (date == null) {
		return 'DateError';
	}

	date = moment(date).utc();
	return date.format('DD MMM YYYY HH:mm:ss');
};
