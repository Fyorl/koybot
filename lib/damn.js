'use strict';

module.exports = function (Context, EventProcessor) {
	var self = this;

	// Seems to be a hard limit set by the server. Need to make sure we never
	// send messages longer than this length otherwise the server forces a
	// disconnect.
	var MAX_MSG_LENGTH = 8064;

	self.States = Object.freeze({
		CONNECTED: 1
		, LOGGING_IN: 2
		, JOINING: 3
		, READY: 4
	});

	self.Commands = Object.freeze({
		DAMN_CLIENT: 'dAmnClient'
		, DAMN_SERVER: 'dAmnServer'
		, LOGIN: 'login'
		, JOIN: 'join'
		, PING: 'ping'
		, PONG: 'pong'
		, SEND: 'send'
		, RECV: 'recv'
		, KICK: 'kick'
		, SET: 'set'
		, PROPERTY: 'property'
	});

	var state = self.States.CONNECTED;
	var timeoutTimer = null;
	var timeoutMillis = -1;

	if (Context.config != null && Context.config.pingTimeoutMinutes != null) {
		timeoutMillis = Context.config.pingTimeoutMinutes * 60 * 1000;
	}

	var checkLogin = function (packet) {
		if (packet.cmd !== self.Commands.LOGIN
			|| packet.param !== Context.config.name
			|| packet.args.e == null) {

			Context.Log.error('Login protocol violation.');
			return;
		}

		if (packet.args.e !== 'ok') {
			Context.Log.error('Login unsuccessful: %s', packet.args.e);
			return;
		}

		Context.Log.log('Login successful.');
		Context.TUIBridge.setup(self);
		join();
	};

	var completeLogin = function (accessToken) {
		Context.Log.log('Access token retrieved.');
		Context.Log.log('Logging in.');

		state = self.States.LOGGING_IN;
		var packet = Context.Packet.create(
			self.Commands.LOGIN
			, Context.config.name
			, {pk: accessToken}
		);

		self.send(packet);
	};

	var handlePacket = function (packet) {
		switch (packet.cmd) {
			case self.Commands.RECV:
				Context.Recv.handle(packet);
				break;

			case self.Commands.PROPERTY:
				Context.Property.handle(packet);
				break;

			default:
				Context.Log.log('Unhandled command: %s', packet.cmd);
		}
	};

	var join = function () {
		if (Context.config.channels.length < 1) {
			Context.Log.log('No channels to join.');
			return;
		}

		Context.Log.log('Joining rooms.');
		state = self.States.READY;

		for (var i = 0; i < Context.config.channels.length; i++) {
			var channel = Context.config.channels[i];

			Context.Log.log('Joining #%s...', channel);
			var packet = Context.Packet.create(
				self.Commands.JOIN
				, 'chat:' + channel);
			Context.TUIBridge.joinChannel(channel);
			self.send(packet);
		}
	};

	var login = function (packet) {
		if (packet.cmd !== self.Commands.DAMN_SERVER
			|| packet.param !== '0.3') {

			Context.Log.error('Handshake protocol violation.');
			return;
		}

		Context.Log.log('Handshake complete.');
		Context.Log.log('Getting access token.');
		Context.OAuth.getAccessToken(
			Context.config.oAuthClientID
			, Context.config.oAuthClientSecret
			, completeLogin);
	};

	var pong = function () {
		self.send(self.Commands.PONG);
		if (!Context.cmdLine.prettyLogs) {
			Context.Log.log(self.Commands.PONG);
		}
	};

	var reconnect = function () {
		// We throw an error here which will be picked up by the main
		// uncaughtException handler which should cause the bot to restart.
		throw new Error('Connection to dAmn server timed out.');
	};

	var resetTimeout = function () {
		if (timeoutMillis < 0) {
			return;
		}

		if (timeoutTimer !== null) {
			clearTimeout(timeoutTimer);
		}

		timeoutTimer = setTimeout(reconnect, timeoutMillis);
	};

	self.send = function (msg) {
		Context.client.write(msg + '\n\0');
	};

	self.setProperty = function (channel, property, value) {
		if (value.length > MAX_MSG_LENGTH) {
			value = value.substr(0, MAX_MSG_LENGTH);
		}

		var packet = Context.Packet.create(
			self.Commands.SET
			, 'chat:' + channel
			, {p: property}
			, value);

		self.send(packet);
	};

	self.setTopic = function (channel, value) {
		self.setProperty(channel, 'topic', value);
	};

	self.sendGenericMsg = function (type, channel, msg) {
		var chunks = Math.ceil(msg.length / MAX_MSG_LENGTH);
		for (var n = 0; n < chunks; n++) {
			var chunk = msg.substr(n * MAX_MSG_LENGTH, MAX_MSG_LENGTH);
			if (chunk.length < 1) {
				break;
			}

			var subPacket = Context.Packet.create('msg', 'main', {}, chunk);
			var packet = Context.Packet.create(
				self.Commands.SEND
				, type + ':' + channel
				, {}
				, subPacket);

			self.send(packet);
		}
	};

	self.sendAction = function (channel, msg) {
		var subPacket = Context.Packet.create('action', 'main', {}, msg);
		var packet = Context.Packet.create(
			self.Commands.SEND
			, 'chat:' + channel
			, {}
			, subPacket);

		self.send(packet);
	};

	self.sendMsg = function (channel, msg) {
		self.sendGenericMsg('chat', channel, msg);
	};

	self.sendPrivMsg = function (to, msg) {
		var pchat = [Context.config.name, to].sort().join(':');
		self.sendGenericMsg('pchat', pchat, msg);
	};

	self.kick = function (channel, target, reason) {
		var packet = Context.Packet.create(
			self.Commands.KICK
			, 'chat:' + channel
			, {u: target}
			, reason);

		self.send(packet);
	};

	self.handshake = function () {
		if (Context.client == null || Context.config == null) {
			Context.Log.error(
				'No client or config data passed to login method.');
			return;
		}

		var packet = Context.Packet.create(
			self.Commands.DAMN_CLIENT
			, '0.3'
			, {
				'agent': Context.config.agentName
			}
		);

		self.send(packet);
	};

	self.recv = function (data) {
		var packet = Context.Packet.parse(data);

		if (packet == null) {
			Context.Log.error('Unable to parse packet.');
			return;
		}

		resetTimeout();

		if (packet.cmd === self.Commands.PING) {
			// Bypass the state machine for pings.
			pong();
			return;
		}

		switch (state) {
			case self.States.CONNECTED:
				login(packet);
				break;

			case self.States.LOGGING_IN:
				checkLogin(packet);
				break;

			case self.States.READY:
				handlePacket(packet);
				break;

			default:
				Context.Log.error(
					'Client is in inconsistent state: %s'
					, state);
		}
	};
};
