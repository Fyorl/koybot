'use strict';

exports.create = function (size) {
	var util = require('util');
	var moment = require('moment');
	var EventEmitter = require('events').EventEmitter;

	var Cache = function (size) {
		var self = this;
		EventEmitter.call(self);

		var objects = 0;
		var cache = {};
		var lastRetrieved = {};

		var getOldestKey = function () {
			var minKey = null;
			var minTimestamp = Infinity;

			for (var key in lastRetrieved) {
				if (lastRetrieved[key] < minTimestamp) {
					minTimestamp = lastRetrieved[key];
					minKey = key;
				}
			}

			return minKey;
		};

		self.put = function (key, value) {
			if (cache[key] !== undefined) {
				cache[key] = value;
			} else {
				if (objects + 1 > size) {
					var oldestKey = getOldestKey();
					self.emit('pop', oldestKey, cache[oldestKey]);
					delete cache[oldestKey];
					delete lastRetrieved[oldestKey];
					objects--;
				}

				cache[key] = value;
				lastRetrieved[key] = moment().utc().valueOf();
				objects++;
			}
		};

		self.get = function (key) {
			if (cache[key] !== undefined) {
				lastRetrieved[key] = moment().utc().valueOf();
			}

			return cache[key];
		};
	};

	util.inherits(Cache, EventEmitter);
	return new Cache(size);
};
