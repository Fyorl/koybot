'use strict';

Function.prototype.curry = function () {
	var fn = this;
	var args = [].slice.call(arguments);
	return function () {
		var myArgs = [].slice.call(arguments);
		return fn.apply(this, args.concat(myArgs));
	};
};
