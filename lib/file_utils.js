'use strict';

var fmt = require('util').format;
var fs = require('fs');
var path = require('path');
var spawn = require('child_process').spawn;
var exec = require('child_process').exec;
var zlib = require('zlib');
var os = require('os');

exports.copyFile = function (src, dest, onComplete) {
	var reader = fs.createReadStream(src);
	var writer = fs.createWriteStream(dest, {mode: 436});
	var error = null;

	writer.on('finish', function () {
		if (error != null) {
			fs.unlink(dest, function () {
				onComplete(error);
			});
		} else {
			onComplete(error);
		}
	});

	writer.on('error', function (err) {
		onComplete(err);
	});

	reader.on('error', function (err) {
		error = err;
		writer.end();
	});

	reader.pipe(writer);
	return true;
};

var deleteContents = function (root, onComplete) {
	var numFiles;
	var numDeleted = 0;

	var deleted = function (err) {
		numDeleted++;
		if (numDeleted >= numFiles) {
			onComplete(err);
		}
	};

	fs.readdir(root, function (err, files) {
		numFiles = files.length;
		if (err != null || numFiles < 1) {
			onComplete(err);
			return;
		}

		files.forEach(function (file) {
			var filepath = path.join(root, file);
			fs.stat(filepath, function (err, stats) {
				if (err != null) {
					deleted(err);
					return;
				}

				if (stats.isFile()) {
					fs.unlink(filepath, deleted);
				} else if (stats.isDirectory()) {
					deleteContents(filepath, function (err) {
						if (err == null) {
							fs.rmdir(filepath, deleted);
						} else {
							deleted(err);
						}
					});
				} else {
					deleted({
						message: "Tried to delete something which wasn't a file or directory."});
				}
			});
		});
	});
};

exports.deleteDir = function (root, onComplete) {
	fs.readdir(root, function (err, files) {
		if (err != null) {
			onComplete(err);
			return;
		}

		if (files.length > 0) {
			deleteContents(root, function (err) {
				if (err != null) {
					onComplete(err);
				} else {
					fs.rmdir(root, onComplete);
				}
			});
		} else {
			fs.rmdir(root, onComplete);
		}
	});
};

exports.unzip = function (file, onComplete) {
	var outFile = file.substring(0, file.length - 4);
	var gunzip = zlib.createGunzip();
	var input = fs.createReadStream(file);
	var output = fs.createWriteStream(outFile, {mode: 436});

	var error = function (err) {
		onComplete(1, outFile);
	};

	input.on('error', error);
	gunzip.on('error', error);
	output.on('error', error);

	output.on('finish', function () {
		fs.unlink(file, function (err) {
			onComplete((err == null) ? 0 : 1, outFile);
		});
	});

	input.pipe(gunzip).pipe(output);
};

exports.zip = function (file, onComplete) {
	var outFile = file + '.bz2';
	var gzip = zlib.createGzip({level: zlib.Z_BEST_COMPRESSION});
	var input = fs.createReadStream(file);
	var output = fs.createWriteStream(outFile, {mode: 436});

	var error = function (err) {
		fs.unlink(outFile);
		onComplete(1, outFile);
	};

	input.on('error', error);
	gzip.on('error', error);
	output.on('error', error);

	output.on('finish', function () {
		fs.unlink(file, function (err) {
			onComplete((err == null) ? 0 : 1, outFile);
		});
	});

	input.pipe(gzip).pipe(output);
};
