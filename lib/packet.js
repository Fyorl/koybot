'use strict';

module.exports = function (Context) {
	var self = this;

	self.create = function (cmd, param, args, body) {
		var packet = [];

		if (cmd != null) {
			if (param == null) {
				packet.push(cmd);
			} else {
				packet.push(cmd + ' ' + param);
			}
		} else {
			Context.Log.error(
				'Unable to create packet: Invalid command.');
			return '';
		}

		if (args != null) {
			for (var key in args) {
				if (!args.hasOwnProperty(key)) {
					continue;
				}

				packet.push(key + '=' + args[key]);
			}
		}

		if (body != null) {
			packet.push('');
			packet.push(body);
		}

		return packet.join('\n');
	};

	self.hasArg = function (packet) {
		var body = packet.body || packet.content || packet;
		var spacePos = body.indexOf(' ');

		if (spacePos < 0) {
			return false;
		}

		var arg = body.substring(spacePos + 1);
		if (arg.length < 1) {
			return false;
		}

		return arg;
	};

	self.extractCmdArgs = function (packet) {
		var body = packet.body || packet.content || packet;

		if (body == null || !body.indexOf) {
			Context.Log.error(
				'Unable to extract command arguments from: %j', packet);
			return [];
		}

		var spacePos = body.indexOf(' ');
		if (spacePos < 0) {
			return [];
		}

		var args = [];
		body = body.substring(spacePos + 1);

		if (body.length < 1) {
			return [];
		}

		var escape = false;
		var quote = false;
		var buf = '';
		for (var i = 0; i < body.length; i++) {
			var c = body.charAt(i);
			switch (c) {
				case ' ':
					if (quote) {
						buf += c;
					} else {
						args.push(buf);
						buf = '';
					}
					escape = false;
					break;

				case '"':
					if (escape) {
						buf += c;
						escape = false;
					} else {
						quote = !quote;
					}
					break;

				case '\\':
					if (escape) {
						buf += c;
						escape = false;
					} else {
						escape = true;
					}
					break;

				default:
					buf += c;
					escape = false;
			}
		}

		if (buf.length > 0) {
			args.push(buf);
		}

		return args;
	};

	self.getChannel = function (packet) {
		if (packet.param == null || packet.param.indexOf(':') < 0) {
			Context.Log.error('Packet has no channel.');
			return null;
		}

		return packet.param.split(':')[1];
	};

	self.parse = function (data) {
		if (data == null) {
			return data;
		}

		var cmd;
		var param;

		var idx = data.search('\n');
		if (idx < 1) {
			return null;
		}

		var headerLine = data.substr(0, idx).replace(/\s*$/, '');
		cmd = headerLine.match(/\S+/)[0];

		var sidx = headerLine.search(' ');
		if (sidx != null && sidx > 0) {
			param = headerLine
				.substr(sidx + 1)
				.match(/\S.*/)[0]
				.replace(/\s*$/, '');
		}

		var args;
		try {
			args = parseArgs(data.substr(idx + 1));
		} catch (e) {
			args = {args: null, data: null};
		}

		return {
			cmd: cmd
			, param: param
			, args: args.args
			, body: trimNull(args.data)
		};
	};

	var parseArgs = function (data) {
		var args = {};
		var body = '';
		var work = data;

		while (work && work.search('\n')) {
			var i = work.search('\n');
			var tmp = work.substr(0, i);
			work = work.substr(i + 1);
			i = tmp.search('=');

			if (i == null || i <= 0) {
				throw new SyntaxError();
			}

			var an = tmp.substr(0, i);
			var av = tmp.substr(i + 1);
			args[an.replace(/\s*$/, '')] = av.replace(/\s*$/, '');
		}

		if (work) {
			body = work.substr(1);
		}

		return {
			args: args
			, data: body
		};
	};

	var trimNull = function (s) {
		if (s != null && s[s.length - 1] === '\u0000') {
			return s.substring(0, s.length - 1);
		}

		return s;
	};
};
