'use strict';

module.exports = function (Context) {
	var self = this;

	var runSQL = function (db, sqlFile, fn) {
		Context.fs.readFile(sqlFile, {encoding: 'utf8'}, function (err, sql) {
			if (err != null) {
				Context.Log.error(
					"Unable to initialise table, error "
					+ "while reading SQL file '%s': %s"
					, sqlFile
					, err.message);
				fn(null);
				return;
			}

			var queries = sql.split(';\n\n').map(function (query) {
				return query.trim();
			});

			var queriesCompleted = 0;
			var failed = false;

			var ranQuery = function (err) {
				queriesCompleted++;

				if (failed) {
					return;
				}

				if (err != null) {
					Context.Log.error(
						'Unable to initialise table, error running SQL: %s'
						, err.message);
					fn(null);
					failed = true;
					return;
				}

				if (queriesCompleted == queries.length) {
					fn(db);
				}
			};

			db.parallelize(function () {
				for (var i = 0; i < queries.length; i++) {
					var query = queries[i];
					db.run(query, ranQuery);
				}
			});
		});
	};

	var initialiseDB = function (db, sqlFile, fn) {
		Context.fs.stat(db.filename, function (err, stats) {
			if (err != null) {
				Context.Log.error(
					'Error checking tables existed: %s', err.message);
				fn(null);
				return;
			}

			if (stats.size < 1) {
				runSQL(db, sqlFile, fn);
			} else {
				fn(db);
			}
		});
	};

	self.load = function (dbFile, sqlFile, fn) {
		var db = new Context.sqlite3.Database(dbFile);

		db.on('error', function (err) {
			Context.Log.error(
				"Error opening '%s': %s"
				, dbFile
				, err.message);

			fn(null);
		});

		db.on('open', function () {
			initialiseDB(db, sqlFile, fn);
		});
	};
};

