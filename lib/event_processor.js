'use strict';

module.exports = function (Context) {
	var self = this;
	var eventMap = {};

	self.fire = function (event) {
		var args = Array.prototype.slice.call(arguments, 1);

		var listeners = eventMap[event];

		if (listeners == null) {
			return;
		}

		for (var i = 0; i < listeners.length; i++) {
			listeners[i].apply(listeners[i], args);
		}
	};

	self.register = function (event, fn) {
		if (event == null || fn == null) {
			Context.Log.error('Insufficient arguments to register listener.');
			return;
		}

		if (typeof fn !== 'function') {
			Context.Log.error('Tried to register a non function listener.');
			return;
		}

		if (!eventMap[event]) {
			eventMap[event] = [];
		}

		eventMap[event].push(fn);
	};

	self.clear = function () {
		eventMap = {};
	};

	self.currentListeners = function () {
		return Object.getOwnPropertyNames(eventMap);
	};
};
