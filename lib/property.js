'use strict';

module.exports = function (Context, EventProcessor) {
	var self = this;
	self.Properties = Object.freeze({
		TOPIC: 'topic'
		, TITLE: 'title'
		, PRIVCLASSES: 'privclasses'
		, MEMBERS: 'members'
	});

	var handleTopic = function (property) {
		EventProcessor.fire('property_topic', property);

		if (Context.cmdLine.prettyLogs) {
			var now = new Date();
			Context.Log.log(
				'%s \u001B[1m** %s changed the topic to:\u001B[0m %s'
				, Context.DateUtils.getPrettyTime(now)
				, property.by
				, property.value);
		}
	};

	self.handle = function (packet) {
		var channel = Context.Packet.getChannel(packet);

		if (channel == null || packet.args == null || packet.args.p == null) {
			Context.Log.error('Corrupt property packet received: %j', packet);
			return;
		}

		var property = {
			channel: channel
			, value: packet.body
		};

		switch (packet.args.p) {
			case self.Properties.TOPIC:
				property.by = packet.args.by;
				handleTopic(property);
				break;

			default:
				Context.Log.log('Unhandled property: %s', packet.args.p);
		}
	};
};
