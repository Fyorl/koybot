'use strict';

exports.randomRange = function (min, max) {
	return Math.random() * (max - min) + min;
};

exports.randomInt = function (min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
};
