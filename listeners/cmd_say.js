'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.say = Context.fmt(
		"Usage:\n"
		+ "	!say <b>text</b> - Ask %s to say something in the current channel."
		, Context.config.name);

	EventProcessor.register('cmd_say', function (recv) {
		var text = Context.Packet.hasArg(recv);
		if (!text) {
			return;
		}

		dAmn.sendMsg(recv.channel, text);
	});
};
