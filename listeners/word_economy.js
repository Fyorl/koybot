'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	// Global transaction lock to avoid data races on the database.
	var transactionLock = Context.Locks.create();

	var DEFAULT_MAX_LINE_LENGTH = 10;
	var DEFAULT_MIN_REWARD = 2;
	var DEFAULT_REWARD_PER_WORD = 1;
	var DEFAULT_TAX_PER_WORD = 2;
	var DEFAULT_KICK_PRICE = 200;
	var DEFAULT_OWN_DISCOUNT = 0.75;
	var DEFAULT_TOPIC_PRICE = 500;

	var maxLineLength = DEFAULT_MAX_LINE_LENGTH;
	var minReward = DEFAULT_MIN_REWARD;
	var rewardPerWord = DEFAULT_REWARD_PER_WORD;
	var taxPerWord = DEFAULT_TAX_PER_WORD;
	var kickPrice = DEFAULT_KICK_PRICE;
	var ownDiscount = DEFAULT_OWN_DISCOUNT;
	var topicPrice = DEFAULT_TOPIC_PRICE;

	if (Context.config != null && Context.config.wordEconomy != null) {
		if (Context.config.wordEconomy.maxLineLength != null) {
			maxLineLength = Context.config.wordEconomy.maxLineLength;
		}

		if (Context.config.wordEconomy.minReward != null) {
			minReward = Context.config.wordEconomy.minReward;
		}

		if (Context.config.wordEconomy.rewardPerWord != null) {
			rewardPerWord = Context.config.wordEconomy.rewardPerWord;
		}

		if (Context.config.wordEconomy.taxPerWord != null) {
			taxPerWord = Context.config.wordEconomy.taxPerWord;
		}

		if (Context.config.wordEconomy.kickPrice != null) {
			kickPrice = Context.config.wordEconomy.kickPrice;
		}

		if (Context.config.wordEconomy.ownDiscount != null) {
			ownDiscount = Context.config.wordEconomy.ownDiscount;
		}

		if (Context.config.wordEconomy.topicPrice != null) {
			topicPrice = Context.config.wordEconomy.topicPrice;
		}
	}

	var getWords = function (msg) {
		return msg.split(/[ ,.!?;:]/).filter(function (x) {
			return x !== '';
		});
	};

	var updateBalances = function (balances) {
		var sqlVars = [];
		var values = {};

		var i = 0;
		for (var player in balances) {
			var sqlVar = Context.fmt('$player_%s', i);
			sqlVars.push(sqlVar);
			values[sqlVar] = player;
			i++;
		}

		var query = Context.fmt(
			'SELECT user, balance '
			+ 'FROM word_economy_players '
			+ 'WHERE user COLLATE NOCASE IN (%s)'
			, sqlVars.join(','));

		Context.maindb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error retrieving balances for update: %s'
					, err.message);

				transactionLock.release();
				return;
			}

			if (rows.length > 0
				&& (rows[0].user == null
					|| rows[0].balance == null)) {

				Context.Log.error(
					'Invalid rows returned when trying to update balances: %j'
					, rows[0]);

				transactionLock.release();
				return;
			}

			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];
				if (balances[row.user.toLowerCase()] == null) {
					Context.Log.error(
						'When updating balances, user "%s" was returned by the '
						+ 'query but was not associated with a balance to '
						+ 'update: %j'
						, row.user
						, balances);

					continue;
				}

				// We have to make sure the username is the correct
				// capitalisation again so we delete the lowercase'd one and
				// replace it with the version that actually came out of the
				// database.
				var tmp = balances[row.user.toLowerCase()];
				delete balances[row.user.toLowerCase()];

				balances[row.user] = tmp + row.balance;
			}

			var vars = [];
			var vals = {};

			var n = 0;
			for (var player in balances) {
				var playerVar = Context.fmt('$player_%s', n);
				var balanceVar = Context.fmt('$balance_%s', n);

				vars.push(
					Context.fmt(
						'((SELECT id FROM word_economy_players '
						+ 'WHERE user COLLATE NOCASE = %s), %s, %s)'
						, playerVar
						, playerVar
						, balanceVar));

				vals[playerVar] = player;
				vals[balanceVar] = balances[player];

				n++;
			}

			if (vars.length < 1) {
				Context.Log.error(
					'When updating balances, nothing ended up being updated.');

				transactionLock.release();
				return;
			}

			var query =
				'REPLACE INTO word_economy_players '
				+ '(id, user, balance) '
				+ 'VALUES '
				+ vars.join(', ');

			Context.maindb.run(query, vals, function (err) {
				transactionLock.release();

				if (err != null) {
					Context.Log.error(
						'Error updating balances: %s'
						, err.message);
				}
			});
		});
	};

	var calculateReward = function (recv, tax, playerTotals) {
		var from = recv.from.toLowerCase();

		if (playerTotals == null) {
			playerTotals = {};
		}

		// At this point we have a list of players whose balances we need to
		// update with the tax they received. In order to reduce the number
		// of database queries we need to do, we can add the player who's
		// getting a reward to this list and update them all at once.

		// This also ensures that a player doesn't receive tax on their own
		// chat line.
		var selfTax = playerTotals[from];

		if (selfTax != null) {
			tax -= selfTax;
			delete playerTotals[from];
		}

		if (tax < 0) {
			tax = 0;
		}

		var words = getWords(recv.content).length;

		if (words > maxLineLength) {
			words = maxLineLength;
		}

		var reward = (words * rewardPerWord);

		if (tax > reward) {
			tax = capTax(reward, tax, playerTotals);
		}

		reward -= tax;

		if (reward < minReward) {
			reward = minReward;
		}

		playerTotals[from] = reward;
		updateBalances(playerTotals);
	};

	var capTax = function (max, totalTax, playerTotals) {
		var totalCappedTax = 0;

		for (var player in playerTotals) {
			var total = playerTotals[player];
			var ratio = total / totalTax;
			var cappedTax = Math.ceil(max * ratio);
			playerTotals[player] = cappedTax;
			totalCappedTax += cappedTax;
		}

		return totalCappedTax;
	};

	var getAndAddTax = function (msg, fn) {
		var words = getWords(msg);

		// We must truncate the number of words to less than 999 due to
		// limitations with the node-sqlite3 package.

		if (words.length > 998) {
			words = words.slice(0, 998);
		}

		var inClause = [];
		var values = {};
		var wordCounts = {};

		for (var i = 0; i < words.length; i++) {
			var word = words[i].toLowerCase();

			if (wordCounts[word] == null) {
				var sqlVar = Context.fmt('$word_%s', i);
				inClause.push(sqlVar);
				values[sqlVar] = word;
				wordCounts[word] = 0;
			}

			wordCounts[word] = wordCounts[word] + 1;
		}

		var query = Context.fmt(
			'SELECT players.user, words.word '
			+ 'FROM word_economy_players AS players, '
			+	'word_economy_words AS words '
			+ 'WHERE words.word IN (%s) '
			+	'AND words.user_id = players.id'
			, inClause.join(','));

		Context.maindb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error calculating tax for words: %s', err.message);

				transactionLock.release();
				return;
			}

			var totalTax = 0;
			var playerTotals = {};

			if (rows.length > 0
				&& (rows[0].user == null
					|| rows[0].word == null)) {

				Context.Log.error(
					'Invalid rows returned when calculating tax for words: %j'
					, rows);

				transactionLock.release();
				return;
			}

			for (var i = 0; i < rows.length; i++) {
				var player = rows[i].user.toLowerCase();
				var word = rows[i].word;
				var wordCount = wordCounts[word];

				if (wordCount == null) {
					Context.Log.error(
						'Word "%s" has no counts associated with it despite '
						+ 'being returned by the query.'
						, word);

					continue;
				}

				if (playerTotals[player] == null) {
					playerTotals[player] = 0;
				}

				playerTotals[player] =
					playerTotals[player] + (wordCount * taxPerWord);
				totalTax += wordCount * taxPerWord;
			}

			fn(totalTax, playerTotals);
		});
	};

	var addLineReward = function (recv) {
		transactionLock.acquire(
			getAndAddTax.curry(
				recv.content
				, calculateReward.curry(recv)));
	};

	EventProcessor.register('recvMsg', function (recv) {
		if (recv.content == null
			|| recv.content.length < 1
			|| recv.from == null) {

			Context.Log.error('Corrupt RECV packet received: %j', recv);
			return;
		}

		if (Context.config == null || Context.config.name == null) {
			Context.Log.error('Invalid config: %j', Context);
			return;
		}

		if (recv.from.toLowerCase() === Context.config.name.toLowerCase()) {
			// Make sure the bot never looks at her own lines.
			return;
		}

		addLineReward(recv);
	});

	var penceToPounds = function (pence) {
		var pounds = '';
		pence = pence.toString();

		if (pence.length < 2) {
			return '&pound;0.0' + pence;
		}

		if (pence.length < 3) {
			return '&pound;0.' + pence;
		}

		for (var i = 0; i < pence.length; i++) {
			var c = pence[pence.length - i - 1];

			if (i < 2) {
				pounds = c + pounds;
			}

			if (i === 2) {
				pounds = c + '.' + pounds;
			}

			if (i > 2) {
				var prepend = c;
				if ((i - 2) % 3 === 0) {
					prepend = c + ',';
				}

				pounds = prepend + pounds;
			}
		}

		return '&pound;' + pounds;
	};

	var getBalance = function (user, fn) {
		var query =
			'SELECT balance '
			+ 'FROM word_economy_players '
			+ 'WHERE user COLLATE NOCASE = $user';

		var values = {$user: user};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error trying to retrieve balance for user "%s": %s'
					, user
					, err.message);

				fn(err);
				return;
			}

			var balance = 0;
			if (row != null && row.balance == null) {
				Context.Log.error(
					'Invalid row returned when retrieving '
					+ 'balance for user "%s".'
					, user);

				fn({message: 'Invalid row returned.'});
				return;
			} else if (row != null && row.balance != null) {
				balance = row.balance;
			}

			fn(balance);
		});
	};

	Context.HelpText.balance =
		"Usage:\n"
		+ "	!balance - Check your balance.";

	EventProcessor.register('cmd_balance', function (recv) {
		if (recv.from == null) {
			return;
		}

		getBalance(recv.from, function (balance) {
			if (typeof balance === 'number') {
				dAmn.sendMsg(
					recv.channel
					, Context.fmt('%s: %s', recv.from, penceToPounds(balance)));
			}
		});
	});

	var calculateWordPrice = function (word) {
		var letters = word.length;

		// Externally configurable?
		if (letters > 20) {
			letters = 20;
		}

		return Math.round(Math.abs(Math.pow((letters-20)/20, 4))*5000)+1;
	};

	var updateBalance = function (user, newBalance, fn) {
		var query =
			'UPDATE word_economy_players '
			+ 'SET balance = $balance '
			+ 'WHERE user COLLATE NOCASE = $user';

		var values = {$balance: newBalance, $user: user};

		Context.maindb.run(query, values, function (err) {
			if (err == null) {
				fn();
			} else {
				transactionLock.release();
				Context.Log.error(
					'Error updating balance for "%s" to %s: %s'
					, user
					, newBalance
					, err.message);
			}
		});
	};

	var checkCanAffordWord = function (recv, word) {
		var price = calculateWordPrice(word);

		getBalance(recv.from, function (balance) {
			if (typeof balance === 'object' && balance.message != null) {
				transactionLock.release();
				return;
			}

			if (price > balance) {
				transactionLock.release();
				dAmn.sendMsg(
					recv.channel
					, Context.fmt(
						'%s: Insufficient funds. "%s" costs %s, you have %s.'
						, recv.from
						, word
						, penceToPounds(price)
						, penceToPounds(balance)));
			} else {
				var newBalance = balance - price;
				updateBalance(recv.from, newBalance, function () {
					var query =
						'INSERT INTO word_economy_words '
						+ '(user_id, word) '
						+	'SELECT id, $word as word '
						+	'FROM word_economy_players '
						+	'WHERE user COLLATE NOCASE = $user';

					var values = {$word: word, $user: recv.from};

					Context.maindb.run(query, values, function (err) {
						transactionLock.release();
						if (err == null) {
							dAmn.sendMsg(
								recv.channel
								, Context.fmt(
									'%s: Purchased "%s" for %s. '
									+ 'Remaining balance is %s.'
									, recv.from
									, word
									, penceToPounds(price)
									, penceToPounds(newBalance)));
						} else {
							Context.Log.error(
								'Error adding "%s" to word list of user "%s": %s'
								, word
								, recv.from
								, err.message);
						}
					});
				});
			}
		});
	};

	var checkBuyWord = function (recv, word) {
		// Check word isn't already bought.
		var query =
			'SELECT players.user '
			+ 'FROM word_economy_players AS players, '
			+	'word_economy_words AS words '
			+ 'WHERE words.word = $word '
			+	'AND words.user_id = players.id';

		var values = {$word: word};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error checking if word "%s" already bought: %s'
					, word
					, err.message);

				transactionLock.release();
				return;
			}

			if (row == null) {
				checkCanAffordWord(recv, word);
			} else {
				transactionLock.release();
				dAmn.sendMsg(
					recv.channel
					, Context.fmt(
						'%s: "%s" is already owned by %s.'
						, recv.from
						, word
						, row.user));
			}
		});
	};

	Context.HelpText.buyword =
		"Usage:\n"
		+ "	!buyword <b>word</b> - Purchase a word if it is not already owned.";

	EventProcessor.register('cmd_buyword', function (recv) {
		if (recv.from == null) {
			return;
		}

		var words = recv.content.split(' ');
		if (words[1] == null || words[1].length < 1) {
			return;
		}

		transactionLock.acquire(
			checkBuyWord.curry(
				recv
				, words[1].toLowerCase()));
	});

	var checkOwnership = function (recv, word) {
		var query =
			'SELECT p.user '
			+ 'FROM word_economy_words AS w, '
			+ 	'word_economy_players AS p '
			+ 'WHERE w.user_id = p.id '
			+	'AND w.word COLLATE NOCASE = $word';

		var values = {$word: word};

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error checking word "%s" ownership: %s'
					, word
					, err.message);

				return;
			}

			var owned = (row != null && row.user != null);
			var user = (owned) ? row.user + '.' : '';

			var msg = Context.fmt(
				'%s: "%s" '
					+ ((owned) ? 'would' : 'will')
					+ ' cost %s to buy'
					+ ((owned) ? ' but is already owned by ' : '.')
				, recv.from
				, word
				, penceToPounds(calculateWordPrice(word)));

			msg += user;
			dAmn.sendMsg(recv.channel, msg);
		});
	};

	Context.HelpText.wordprice =
		"Usage:\n"
		+ "	!wordprice <b>word</b> - States the cost of a word. Also tells "
		+ "	you if someone already owns the word.";

	EventProcessor.register('cmd_wordprice', function (recv) {
		if (recv.from == null) {
			return;
		}

		var words = recv.content.split(' ');
		if (words[1] == null || words[1].length < 1) {
			return;
		}

		checkOwnership(recv, words[1]);
	});

	var givePence = function (from, to, amount, fn) {
		var query =
			'SELECT user, balance '
			+ 'FROM word_economy_players '
			+ 'WHERE user COLLATE NOCASE IN ($from,$to)';

		var values = {$from: from, $to: to};

		Context.maindb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error checking balances for players %s and %s: %s'
					, from
					, to
					, err.message);

				transactionLock.release();
				return;
			}

			var newFromBalance = -1;
			var newToBalance = -1;

			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];

				if (row.user.toLowerCase() === from.toLowerCase()) {
					// Make sure we are placing the correct capitalisation bac
					// into the database.
					from = row.user;

					if (amount > row.balance) {
						transactionLock.release();
						fn(Context.fmt('%s: Insufficient funds.', from));
						return;
					}

					newFromBalance = row.balance - amount;
				} else {
					if (row.user.toLowerCase() === to.toLowerCase()) {
						to = row.user;
					}

					newToBalance = row.balance + amount;
				}
			}

			if (newFromBalance < 0) {
				transactionLock.release();
				fn(Context.fmt('%s: Insufficient funds.', from));
				return;
			}

			if (newToBalance < 0) {
				newToBalance = amount;
			}

			var query =
				'REPLACE INTO word_economy_players '
				+ '(id, user, balance) '
				+ 'VALUES '
				+	'((SELECT id FROM word_economy_players '
				+		'WHERE user COLLATE NOCASE = $from)'
				+		', $from, $frombalance), '
				+	'((SELECT id FROM word_economy_players '
				+		'WHERE user COLLATE NOCASE = $to)'
				+		', $to, $tobalance)';

			var values = {
				$from: from
				, $to: to
				, $frombalance: newFromBalance
				, $tobalance: newToBalance
			};

			Context.maindb.run(query, values, function (err) {
				if (err != null) {
					Context.Log.error(
						'Error updating balances for "%s" and "%s": %s'
						, from
						, to
						, err.message);

					transactionLock.release();
					return;
				}

				fn();
			});
		});
	};

	Context.HelpText.give =
		"Usage:\n"
		+ "	!give <b>username</b> <b>amount</b> - Give a specific user some "
		+ "amount of your money.";

	EventProcessor.register('cmd_give', function (recv) {
		if (recv.from == null) {
			return;
		}

		var args = Context.Packet.extractCmdArgs(recv);

		if (args.length < 2) {
			return;
		}

		var to = args[0];

		if (recv.from.toLowerCase() === to.toLowerCase()) {
			return;
		}

		var amount = args[1].match(/^\D?([0-9.]+)$/);

		if (amount == null || amount[1] == null) {
			return;
		}

		amount = Math.floor(parseFloat(amount[1]) * 100);

		if (isNaN(amount) || amount < 1) {
			return;
		}

		var onComplete = function (err) {
			transactionLock.release();
			if (err == null) {
				dAmn.sendMsg(
					recv.channel
					, Context.fmt(
						'%s: Gave %s to %s.'
						, recv.from
						, penceToPounds(amount)
						, to));
			} else {
				dAmn.sendMsg(recv.channel, err);
			}
		};

		transactionLock.acquire(
			givePence.curry(
				recv.from, to, amount, onComplete));
	});

	Context.HelpText.mywords =
		"Usage:\n"
		+ "	!mywords - Lists the words you own.";

	EventProcessor.register('cmd_mywords', function (recv) {
		if (recv.from == null) {
			return;
		}

		var query =
			'SELECT words.word '
			+ 'FROM word_economy_words AS words, '
			+	'word_economy_players AS players '
			+ 'WHERE players.user COLLATE NOCASE = $user '
			+	'AND words.user_id = players.id';

		var values = {$user: recv.from};

		Context.maindb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error checking words owned by "%s": %s'
					, recv.from
					, err.message);

				return;
			}

			if (rows.length < 1) {
				dAmn.sendMsg(
					recv.channel
					, Context.fmt('%s: You own no words.', recv.from));

				return;
			}

			var words = rows.map(function (row) {
				return Context.fmt('"%s"', Context.Tablumps.convert(row.word));
			});

			var plural = (rows.length === 1) ? 'word' : 'words';
			dAmn.sendMsg(
				recv.channel
				, Context.fmt(
					'%s, you own the following %s: %s'
					, recv.from
					, plural
					, words.join(', ')));
		});
	});

	var buyKick = function (recv, target, reason) {
		getBalance(recv.from, function (balance) {
			if (typeof balance === 'object' && balance.message != null) {
				transactionLock.release();
				return;
			}

			var query =
				'SELECT count(*) '
				+ 'FROM word_economy_players AS players, '
				+	'word_economy_words AS words '
				+ 'WHERE players.user = $user '
				+	'AND words.user_id = players.id '
				+	'AND words.word = $word '
				+ 'COLLATE NOCASE';

			var values = {$user: recv.from, $word: target};

			Context.maindb.get(query, values, function (err, row) {
				if (err != null) {
					transactionLock.release();
					Context.Log.error(
						'Error checking if "%s" was owned by "%s": %s'
						, target
						, recv.from
						, err.message);

					return;
				}

				if (row == null || row['count(*)'] == null) {
					transactionLock.release();
					Context.Log.error(
						'Invalid row returned when checking if '
						+ '"%s" was owned by "%s": %j'
						, target
						, recv.from
						, row);

					return;
				}

				var cost = kickPrice;
				if (row['count(*)'] > 0) {
					cost *= ownDiscount;
				}

				if (balance < cost) {
					transactionLock.release();
					dAmn.sendMsg(
						recv.channel
						, Context.fmt(
							'%s: Insufficient funds. Kicking %s will cost %s.'
							, recv.from
							, target
							, penceToPounds(cost)));

					return;
				}

				updateBalance(recv.from, balance - cost, function () {
					transactionLock.release();
					dAmn.kick(recv.channel, target, reason);
				});
			});
		});
	};

	Context.HelpText.buykick =
		"Usage:\n"
		+ "	!buykick <b>username</b> - Kick a specific user. Currently costs "
		+ penceToPounds(kickPrice) + " or "
		+ penceToPounds(kickPrice * ownDiscount) + " if you own the user.";

	EventProcessor.register('cmd_buykick', function (recv) {
		if (recv.from == null) {
			return;
		}

		var words = recv.content.split(' ');

		if (words.length < 2) {
			return;
		}

		var target = words[1];

		if (target.toLowerCase() === Context.config.name.toLowerCase()) {
			// Can't kick herself.
			return;
		}

		var reason = null;

		if (words.length > 2) {
			words.shift();
			words.shift();
			reason = words.join(' ');
		}

		transactionLock.acquire(buyKick.curry(recv, target, reason));
	});

	var buyTopic = function (recv, topic) {
		getBalance(recv.from, function (balance) {
			if (typeof balance === 'object' && balance.message != null) {
				transactionLock.release();
				return;
			}

			if (balance < topicPrice) {
				transactionLock.release();
				dAmn.sendMsg(
					recv.channel
					, Context.fmt(
						'%s: Insufficient funds. '
						+ 'Purchasing a new topic costs %s.'
						, recv.from
						, penceToPounds(topicPrice)));

				return;
			}

			updateBalance(recv.from, balance - topicPrice, function () {
				transactionLock.release();
				dAmn.setTopic(recv.channel, topic);

				var lastTopicChange = {
					by: recv.from
					, at: Context.moment().utc().valueOf()
				};

				Context.memory.lastTopicChange = lastTopicChange;
				Context.KeyValueStore.put('LastTopicChange', lastTopicChange);
			});
		});
	};

	Context.HelpText.buytopic =
		"Usage:\n"
		+ "	!buytopic <b>topic</b> - Set a new topic. Currently costs "
		+ penceToPounds(topicPrice) + ".";

	EventProcessor.register('cmd_buytopic', function (recv) {
		if (recv.from == null) {
			return;
		}

		var topic = Context.Packet.hasArg(recv);
		if (!topic) {
			return;
		}

		topic = Context.Tablumps.convert(topic);
		transactionLock.acquire(buyTopic.curry(recv, topic));
	});
};
