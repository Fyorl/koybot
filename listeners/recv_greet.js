'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var self = this;
	var DEFAULT_QUOTE_CHANCE = 0.2;

	var quoteChance = DEFAULT_QUOTE_CHANCE;
	if (Context.config != null
		&& Context.config.quoteChance != null
		&& typeof Context.config.quoteChance === 'number'
		&& Context.config.quoteChance <= 1
		&& Context.config.quoteChance >= 0) {

		quoteChance = Context.config.quoteChance;
	}

	var greet = function (recv, quoteString) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'Hello, %s!%s'
				, recv.from
				, Context.Tablumps.convert(quoteString)));
	};

	var saveRecentQuote = function (quoteID) {
		Context.memory.recentQuoteID = quoteID;
		Context.KeyValueStore.put('RecentQuoteID', quoteID);
	};

	var pullQuote = function (recv) {
		var query =
			'SELECT id, quote '
			+ 'FROM quotes '
			+ 'WHERE quoted = $quoted';

		var values = {$quoted: recv.from};

		Context.maindb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Unable to get list of quotes for user %s: %s'
					, recv.from
					, err.message);
				return;
			}

			var quoteString = '';
			if (rows != null && rows.length > 0) {
				var index = Context.MathsUtils.randomInt(0, rows.length - 1);
				var row = rows[index];

				if (row != null && row.quote != null && row.id != null) {
					saveRecentQuote(row.id);
					quoteString = Context.fmt(' "%s"', row.quote);
				}
			}

			greet(recv, quoteString);
		});
	};

	EventProcessor.register('recvJoin', function (recv) {
		if (quoteChance > 0) {
			if (Math.random() < quoteChance) {
				pullQuote(recv);
				return;
			}
		}

		greet(recv, '');
	});
};
