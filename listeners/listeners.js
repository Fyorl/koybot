'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var listenerDir = 'listeners';
	if (Context.config != null && Context.config.listenerDirectory != null) {
		listenerDir = Context.config.listenerDirectory;
	}

	var accessDenied = function (recv) {
		dAmn.sendMsg(recv.channel, 'Access denied.');
	};

	var checkExists = function (module, fn) {
		Context.fs.stat(module, function (err, stat) {
			fn(err);
		});
	};

	var notFound = function (recv, module, verb) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'Module "%s" not found or already %s.'
				, module
				, verb));
	};

	var getModuleFilename = function (module) {
		return Context.fmt('%s/%s.js', listenerDir, module);
	};

	var getDisabledFilename = function (module) {
		return Context.fmt('%s/disabled/%s.js', listenerDir, module);
	};

	var moveModule = function (src, dest, fn) {
		Context.FileUtils.copyFile(src, dest, function (err) {
			if (err != null) {
				fn(err);
				return;
			}

			Context.fs.unlink(src, function (err) {
				fn(err);
			});
		});
	};

	var reload = function (recv) {
		recv.content = Context.config.cmdChar[0] + 'reload';
		EventProcessor.fire('cmd_reload', recv);
	};

	var enable = function (recv, module) {
		var moduleFilename = getModuleFilename(module);
		var disabledFilename = getDisabledFilename(module);

		checkExists(disabledFilename, function (err) {
			if (err != null) {
				notFound(recv, module, 'enabled');
				return;
			}

			moveModule(disabledFilename, moduleFilename, function (err) {
				if (err != null) {
					Context.Log.error(
						'Error moving "%s" to "%s": %s'
						, disabledFilename
						, moduleFilename
						, err.message);

					return;
				}

				dAmn.sendMsg(
					recv.channel, Context.fmt('Module "%s" enabled.', module));

				reload(recv);
			});
		});
	};

	var disable = function (recv, module) {
		var moduleFilename = getModuleFilename(module);
		var disabledFilename = getDisabledFilename(module);

		checkExists(moduleFilename, function (err) {
			if (err != null) {
				notFound(recv, module, 'disabled');
				return;
			}

			moveModule(moduleFilename, disabledFilename, function (err) {
				if (err != null) {
					Context.Log.error(
						'Error moving "%s" to "%s": %s'
						, moduleFilename
						, disabledFilename
						, err.message);

					return;
				}

				dAmn.sendMsg(
					recv.channel, Context.fmt('Module "%s" disabled.', module));

				reload(recv);
			});
		});
	};

	var checkAdmin = function (recv, fn, module, err, isAdmin) {
		if (err != null) {
			Context.Log.error(
				'Unable to check if "%s" has admin privileges '
				+ 'to run enable/disable command: %s'
				, recv.from
				, err.message);

			return;
		}

		if (isAdmin) {
			fn(recv, module);
		} else {
			accessDenied(recv);
		}
	};

	Context.HelpText.enable =
		'Usage:\n'
		+ '	!enable <b>module</b> - Enables the listener module specified '
		+ 'and any commands contained within it.';

	EventProcessor.register('cmd_enable', function (recv) {
		var module = Context.Packet.hasArg(recv);
		if (!module) {
			return;
		}

		Context.Admin.isAdmin(
			Context.maindb
			, recv.from
			, checkAdmin.curry(recv, enable, module));
	});

	Context.HelpText.disable =
		'Usage:\n'
		+ '	!disable <b>module</b> - Disables the listener module specified '
		+ 'and any commands contained within it.';

	EventProcessor.register('cmd_disable', function (recv) {
		var module = Context.Packet.hasArg(recv);
		if (!module) {
			return;
		}

		Context.Admin.isAdmin(
			Context.maindb
			, recv.from
			, checkAdmin.curry(recv, disable, module));
	});
};

