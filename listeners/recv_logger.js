'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	var self = this;
	var rotating = false;
	var transferFailed = false;

	var DEFAULT_PRESERVED_ROWS = 100;
	var DEFAULT_LOG_DIR = 'logs';
	var DB_KEY_LAST_LOG = 'LastLogRotation';

	// Due to limitations in the node-sqlite3 package and the inability to
	// change the value for SQLITE_MAX_VARIABLE_NUMBER, the following value,
	// multiplied by the number of columns in the log database, must be less
	// than 999. Currently there are 4 columns so we have 240 * 4 = 960. Do not
	// change unless you know what you are doing.
	var DEFAULT_COPY_ROWS = 240;

	var logDir = DEFAULT_LOG_DIR;
	if (Context.config != null && Context.config.logDirectory != null) {
		logDir = Context.config.logDirectory;
	}

	var getLogPath = function (date) {
		return Context.fmt(
			'%s/%s.%d.log.sq3'
			, logDir
			, date.format('YYYY/MM/DD')
			, date.valueOf());
	};

	var truncateLogDB = function () {
		var preserveRows = DEFAULT_PRESERVED_ROWS;

		if (Context.config != null
			&& Context.config.rotateLogsPreserve != null) {

			preserveRows = Context.config.rotateLogsPreserve;
		}

		var query =
			'SELECT timestamp '
			+ 'FROM log '
			+ 'ORDER BY timestamp DESC '
			+ 'LIMIT $limit';

		var values = {$limit: preserveRows};

		Context.logdb.all(query, values, function (err, rows) {
			if (err != null || rows.length < 1) {
				Context.Log.error(
					'Error determining timestamp to preserve '
					+ 'during log rotation: %s'
					, (err == null) ? 'no rows returned' : err.message);

				rotating = false;
				return;
			}

			var lastTimestamp = rows[rows.length - 1].timestamp;
			var today = Context.moment().utc().startOf('day').valueOf();
			if (lastTimestamp > today) {
				lastTimestamp = today;
			}

			var query =
				'DELETE FROM log '
				+ 'WHERE timestamp < $timestamp';

			var values = {$timestamp: lastTimestamp};

			Context.logdb.run(query, values, function (err) {
				if (err != null) {
					Context.Log.error(
						'Unable to remove rotated rows from log DB: %s'
						, err.message);

					rotating = false;
					return;
				}

				var query =
					'UPDATE misc '
					+ 'SET value = $now '
					+ 'WHERE key = $key';

				var values = {
					$now: today
					, $key: DB_KEY_LAST_LOG
				};

				Context.maindb.run(query, values, function (err) {
					rotating = false;

					if (err != null) {
						Context.Log.error(
							'Unable to update last log rotation time: %s'
							, err.message);
					}
				});
			});
		});
	};

	var bzipLogs = function (tmpdbs) {
		var bzips = 0;

		var onBzipExit = function (code, tmpdb) {
			bzips++;
			if (code !== 0) {
				Context.Log.error(
					"Unable to compress rotated log file '%s'. "
					+ "Please compress and move manually."
					, tmpdb);
			}

			if (bzips >= tmpdbs.length) {
				truncateLogDB();
			}
		};

		for (var i = 0; i < tmpdbs.length; i++) {
			var tmpdb = tmpdbs[i];
			Context.FileUtils.zip(tmpdb, onBzipExit);
		}
	};

	var doCopy = function (offset
	                       , limit
	                       , tmpdb
	                       , tmpdbName
	                       , start
	                       , end
	                       , onComplete) {

		var done = function () {
			tmpdb.close();

			tmpdb.on('error', function (err) {
				Context.Log.error(
					'Unable to close temporary log database %s: %s'
					, tmpdbName
					, err.message);
			});

			onComplete();
		};

		var query =
			'SELECT * '
			+ 'FROM log '
			+ 'WHERE timestamp >= $start '
			+	'AND timestamp < $end '
			+ 'LIMIT $limit '
			+ 'OFFSET $offset';

		var values = {
			$start: start
			, $end: end
			, $limit: limit
			, $offset: offset
		};

		Context.logdb.all(query, values, function (err, rows) {
			if (err != null) {
				Context.Log.error(
					'Error transferring chunk of data '
					+ 'to temporary log DB: %s'
					, err.message);

				transferFailed = true;
				onComplete();
				return;
			}

			if (rows.length < 1) {
				done();
				return;
			}

			var query =
				'INSERT INTO log '
				+ '(timestamp, channel, user, subpacket) '
				+ 'VALUES ';

			var values = {};
			var params = [];
			for (var i = 0; i < rows.length; i++) {
				var row = rows[i];
				values['$timestamp' + i] = row.timestamp;
				values['$channel' + i] = row.channel;
				values['$user' + i] = row.user;
				values['$subpacket' + i] = row.subpacket;

				params.push(
					Context.fmt(
						'($timestamp%d, $channel%d'
						+ ', $user%d, $subpacket%d)'
						, i, i, i, i));
			}

			query += params.join(', ');
			tmpdb.run(query, values, function (err) {
				if (err != null) {
					Context.Log.error(
						'Error transferring chunk of data '
						+ 'to temporary log DB: %s'
						, err.message);

					transferFailed = true;
					onComplete();
					return;
				}

				offset += limit;
				doCopy(offset, limit, tmpdb, tmpdbName, start, end, onComplete);
			});
		});
	};

	var copyRows = function (tmpdb, tmpLogDBName, start, end, onComplete) {
		doCopy(
			0
			, DEFAULT_COPY_ROWS
			, tmpdb
			, tmpLogDBName
			, start
			, end
			, onComplete);
	};

	var deleteFailedTmpDBs = function (tmpdbs) {
		var onUnlink = function (tmpdb, err) {
			if (err != null) {
				Context.Log.error(
					'Unable to delete %s: %s. Please delete manually.'
					, tmpdb
					, err.message);
			}
		};

		for (var i = 0; i < tmpdbs.length; i++) {
			var tmpdb = tmpdbs[i];
			Context.fs.unlink(tmpdb, onUnlink.curry(tmpdb));
		}
	};

	var doRotate = function () {
		if (rotating) {
			return;
		}

		rotating = true;
		transferFailed = false;
		var transfers = [];

		var transfer = function (totalTransfers
		                         , startTimestamp
		                         , endTimestamp
		                         , tmpdbName) {

			var checkDone = function () {
				transfers.push(tmpdbName);
				if (transfers.length >= totalTransfers) {
					if (transferFailed) {
						rotating = false;
						deleteFailedTmpDBs(transfers);
					} else {
						bzipLogs(transfers);
					}
				}
			};

			Context.DatabaseLoader.load(
				tmpdbName
				, 'db/log.sql'
				, function (db) {
					if (db == null) {
						Context.Log.error(
							'Error creating temporary database %s.'
							, tmpdbName);

						transferFailed = true;
						checkDone();
						return;
					}

					copyRows(
						db
						, tmpdbName
						, startTimestamp
						, endTimestamp
						, checkDone);
				}
			);
		};

		var rotate = function (days) {
			var onMkdirMonth = function (day, err) {
				var logpath = getLogPath(day);
				transfer(
					days.length
					, day.valueOf()
					, day.add(1, 'day').startOf('day').valueOf()
					, logpath);
			};

			var onMkdirYear = function (day, logpathYear, err) {
				var logpathMonth =
					Context.fmt('%s/%s', logpathYear, day.format('MM'));
				Context.fs.mkdir(logpathMonth, 509, onMkdirMonth.curry(day));
			};

			for (var i = 0; i < days.length; i++) {
				var day = Context.moment(days[i]).utc();
				var logpathYear =
					Context.fmt('%s/%s', logDir, day.format('YYYY'));

				// 509 is chmod mode 0775 but octals are not allowed in strict
				// mode.
				Context.fs.mkdir(
					logpathYear
					, 509
					, onMkdirYear.curry(day, logpathYear));
			}
		};

		var daysToRotate = function (earliestTimestamp) {
			var today = Context.moment().utc().startOf('day');
			var earliestDay =
				Context.moment(earliestTimestamp).utc().startOf('day');

			var dayDifference = today.diff(earliestDay, 'days');

			if (dayDifference < 1 || earliestTimestamp > today.valueOf()) {
				// Why are we here?
				rotating = false;
				return;
			}

			var daysToLog = [];
			var daysChecked = 0;

			var checkLogFile = function (timestamp, err, stats) {
				if (err != null && err.code != null && err.code === 'ENOENT') {
					daysToLog.push(timestamp);
				}

				daysChecked++;
				if (daysChecked >= dayDifference) {
					rotate(daysToLog);
				}
			};

			while (today.diff(earliestDay, 'days') > 0) {
				var logpath = getLogPath(earliestDay) + '.bz2';
				Context.fs.stat(
					logpath
					, checkLogFile.curry(earliestDay.valueOf()));
				earliestDay.add(1, 'day').startOf('day');
			}
		};

		var query =
			'SELECT timestamp '
			+ 'FROM log '
			+ 'ORDER BY timestamp ASC '
			+ 'LIMIT 1';

		Context.logdb.get(query, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Error checking earliest log timestamp: %s'
					, err.message);

				rotating = false;
				return;
			}

			if (row == null || row.timestamp == null) {
				Context.Log.error('No rows in log database.');
				rotating = false;
				return;
			}

			daysToRotate(row.timestamp);
		});
	};

	var rotateLogs = function () {
		if (rotating) {
			return;
		}

		var today = Context.moment().utc().startOf('day').valueOf();

		var fallbackCheck = function () {
			var yesterday =
				Context.moment().utc().subtract(1, 'day').startOf('day');
			var logpath = getLogPath(yesterday) + '.bz2';

			Context.fs.stat(logpath, function (err, stats) {
				if (err != null) {
					// Yesterday's log file doesn't exist so we need to rotate.
					doRotate();
				}
			});
		};

		var initialiseLastLogKey = function () {
			var query =
				'INSERT INTO misc '
				+ '(key, value) '
				+ 'VALUES ($key, $value)';

			var values = {$key: DB_KEY_LAST_LOG, $value: 0};

			Context.maindb.run(query, values, function (err) {
				if (err != null) {
					Context.Log.error(
						'Unable to store last log time: %s'
						, err.message);

					return;
				}

				fallbackCheck();
			});
		};

		var checkRotateByKey = function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to check if logs should rotate by key, '
					+ 'trying by time: %s'
					, err.message);

				fallbackCheck();
				return;
			}

			if (row == null || !row.value) {
				initialiseLastLogKey();
				return;
			}

			var lastCheckTimestamp;
			try {
				lastCheckTimestamp = parseInt(row.value);
			} catch (e) {
				err = e;
			}

			if (err != null || isNaN(lastCheckTimestamp)) {
				Context.Log.error(
					'Error converting stored timestamp to number, database '
					+ 'may be corrupt: %s'
					, (err == null ? '' : err.message));

				fallbackCheck();
				return;
			}

			if (lastCheckTimestamp < today) {
				doRotate();
			}
		};

		var query =
			'SELECT value '
			+ 'FROM misc '
			+ 'WHERE key = $key';

		var values = {$key: DB_KEY_LAST_LOG};
		Context.maindb.get(query, values, checkRotateByKey);
	};

	EventProcessor.register('recv', function (channel, subPacket, body) {
		var user = null;
		if (subPacket.args != null && subPacket.args.from != null) {
			user = subPacket.args.from;
		}

		var query =
			'INSERT INTO log '
			+ '(timestamp, channel, user, subpacket) '
			+ 'VALUES($timestamp, $channel, $user, $subpacket)';

		var values = {
			$timestamp: Context.moment().utc().valueOf()
			, $channel: channel
			, $user: user
			, $subpacket: body
		};

		Context.logdb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error('Error logging packet: %s', err.message);
			}
		});

		rotateLogs();
	});
};
