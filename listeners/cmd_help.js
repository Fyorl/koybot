'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.help =
		"Usage:\n"
		+ "	!help - Print a list of available commands.\n"
		+ "	!help <b>command</b> - Print detailed usage instructions for the "
		+ "specific command.";

	var formatHelpText = function (text) {
		if (text == null) {
			return null;
		}

		var newline = '<br/>';
		var tab = '&nbsp;&nbsp;&nbsp;&nbsp;';

		text = text.replace(/\n/g, newline);
		text = text.replace(/\t/g, tab);

		if (Context.config != null
			&& Context.config.cmdChar != null
			&& Context.config.cmdChar.length > 0) {

			text = text.replace(/!/g, Context.config.cmdChar[0]);
		}

		return text;
	};

	EventProcessor.register('cmd_help', function (recv) {
		var output = '';
		var splitOnSpace = recv.content.split(' ');
		if (splitOnSpace.length > 1) {
			var command = splitOnSpace[1];
			output = formatHelpText(Context.HelpText[command.toLowerCase()]);

			if (output == null) {
				return;
			}
		} else {
			var currentListeners = EventProcessor.currentListeners();
			var commands =
			currentListeners.filter(function (listenerName) {
				return listenerName.indexOf('cmd_') > -1;
			}).map(function (listenerName) {
				return Context.fmt('<b>%s</b>', listenerName.substring(4));
			});

			output = Context.fmt(
				'Use !help <b>command</b> to get help on a specific command. '
				+ 'Currently available commands are: %s'
				, commands.join(', '));
		}

		dAmn.sendMsg(recv.channel, output);
	});
};
