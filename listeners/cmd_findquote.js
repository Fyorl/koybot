'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.findquote =
		"Usage:\n"
		+ "	!findquote #<b>ID</b> - Retrieves the quote with a specific "
		+ "ID number.\n"
		+ "	!findquote <b>search</b> - Retrieves the most recent quote "
		+ "containing the whole search term.\n"
		+ "	!findquote &lt;<b>username</b>&gt; <b>search</b> - Retrieves the "
		+ "most recent quote of a specific user that contains the whole "
		+ "search term.";

	var sendResult = function (recv, row) {
		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'Quote #%d, quoted by %s on %s: &lt;%s&gt; %s'
				, row.id
				, row.quoter
				, Context.DateUtils.getPrettyDate(new Date(row.time_quoted))
				, row.quoted
				, Context.Tablumps.convert(row.quote)));
	};

	var findQuote = function (recv, id, user, searchString) {
		var idFilter = '';
		var userFilter = '';
		var searchFilter = '';
		var values = {};

		if (id != null) {
			idFilter = ' WHERE id = $id';
			values.$id = id;
		}

		if (searchString != null) {
			searchFilter = " WHERE quote LIKE $quote";
			values.$quote = Context.fmt("%%%s%%", searchString);
		}

		if (user != null) {
			userFilter = ' AND quoted = $quoted';
			values.$quoted = user;
		}

		var query =
			'SELECT id, time_quoted, quoter, quoted, quote '
			+ 'FROM quotes'
			+ idFilter
			+ searchFilter
			+ userFilter
			+ ' ORDER BY time_quoted DESC'
			+ ' LIMIT 1';

		Context.maindb.get(query, values, function (err, row) {
			if (err != null) {
				Context.Log.error(
					'Unable to search for quotes: %s'
					, err.message);
				return;
			}

			if (row == null) {
				return;
			}

			sendResult(recv, row);
		});
	};

	EventProcessor.register('cmd_findquote', function (recv) {
		var searchString = Context.Packet.hasArg(recv);
		if (!searchString) {
			return;
		}

		var idMatch = searchString.match(/^#(\d+)/);
		if (idMatch != null && idMatch[1] != null) {
			findQuote(recv, idMatch[1], null, null);
			return;
		}

		var user = null;
		var userMatch = searchString.match(/^&lt;([^&]+)&gt;/);
		if (userMatch != null && userMatch[1] != null) {
			user = userMatch[1];
			searchString = Context.Packet.hasArg(searchString);

			if (!searchString) {
				return;
			}
		}

		findQuote(recv, null, user, searchString);
	});
};
