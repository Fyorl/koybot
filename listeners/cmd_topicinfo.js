'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	EventProcessor.register('property_topic', function (property) {
		if (property != null
			&& property.by != null
			&& Context.config != null
			&& Context.config.name != null
			&& Context.config.name.toLowerCase()
				!== property.by.toLowerCase()) {

			var lastTopicChange = {
				by: property.by
				, at: Context.moment().utc().valueOf()
			};

			Context.memory.lastTopicChange = lastTopicChange;
			Context.KeyValueStore.put('LastTopicChange', lastTopicChange);
		}
	});

	EventProcessor.register('init', function () {
		// By using the key-value store we can ensure that information stored
		// in memory is not lost if the bot crashes or loses connection and
		// has to shut down.

		Context.KeyValueStore.get('LastTopicChange', function (value) {
			try {
				Context.memory.lastTopicChange = JSON.parse(value);
			} catch (e) {
				Context.Log.error(
					'Corrupt JSON retrieved when loading '
					+ 'last topic state: %s\n%s'
					, value
					, e.message);
			}
		});
	});

	Context.HelpText.topicinfo =
		"Usage:\n"
		+ "	!topicinfo - Gives details about the last topic change.";

	EventProcessor.register('cmd_topicinfo', function (recv) {
		if (Context.memory == null || Context.memory.lastTopicChange == null) {
			return;
		}

		var topicinfo = Context.memory.lastTopicChange;
		var date = Context.moment(topicinfo.at).utc().toDate();

		dAmn.sendMsg(
			recv.channel
			, Context.fmt(
				'The topic was last changed by %s on %s.'
				, topicinfo.by
				, Context.DateUtils.getPrettyDate(date)));
	});
};
