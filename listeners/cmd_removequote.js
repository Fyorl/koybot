'use strict';

module.exports = function (Context, EventProcessor, dAmn) {
	Context.HelpText.removequote =
		"Usage:\n"
		+ "	!removequote #<b>ID</b> - Deletes a quote with a specific ID from "
		+ "the database if you are an admin or if you quoted it.";

	var accessDenied = function (recv) {
		dAmn.sendMsg(recv.channel, 'Access denied.');
	};

	var removeQuote = function (recv, quoteID) {
		var query =
			'DELETE FROM quotes '
			+ 'WHERE id = $id';

		var values = {$id: quoteID};

		Context.maindb.run(query, values, function (err) {
			if (err != null) {
				Context.Log.error(
					'Error removing quote #%s: %s'
					, quoteID
					, err.message);

				return;
			}

			dAmn.sendMsg(
				recv.channel
				, Context.fmt('Quote #%s removed.', quoteID));
		});
	};

	var checkQuoter = function (recv, quoteID, err, row) {
		if (err != null) {
			Context.Log.error(
				'Error checking quoter when removing quote #%s: %s'
				, quoteID
				, err.message);

			return;
		}

		if (row == null || row['count(*)'] == null) {
			Context.Log.error(
				'Invalid row returned when checking quoter for quote #%s: %j'
				, quoteID
				, row);

			return;
		}

		if (row['count(*)'] > 0) {
			removeQuote(recv, quoteID);
		} else {
			accessDenied(recv);
		}
	};

	var checkAdmin = function (recv, quoteID, err, isAdmin) {
		if (err != null) {
			Context.Log.error(
				'Error checking admin status when removing quote #%s: %s'
				, quoteID
				, err.message);

			return;
		}

		if (isAdmin) {
			removeQuote(recv, quoteID);
		} else {
			// If we're not an admin, check if we quoted it.
			var query =
				'SELECT count(*) '
				+ 'FROM quotes '
				+ 'WHERE quoter COLLATE NOCASE = $quoter '
				+ 	'AND id = $id';

			var values = {$quoter: recv.from, $id: quoteID};

			Context.maindb.get(query, values, checkQuoter.curry(recv, quoteID));
		}
	};

	EventProcessor.register('cmd_removequote', function (recv) {
		var quoteID = recv.content.match(/^[^ ]+ #?(\d+)$/);
		if (quoteID != null && quoteID[1] != null) {
			Context.Admin.isAdmin(
				Context.maindb
				, recv.from
				, checkAdmin.curry(recv, quoteID[1]));
		}
	});
};
